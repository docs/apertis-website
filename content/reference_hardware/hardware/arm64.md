+++
date = "2020-06-10"
weight = 100

title = "ARM 64-bit Reference Hardware"

aliases = [
    "/reference_hardware/arm64"
]
+++

Starting with the 17.03 release, ARM64 images have been made available. While
the images in principle support various 64 bit ARM boards, Renesas Generation 3
SoCs are the main target.

The recommend reference boards for these images are the R-Car Starter Kit
boards, however the Salvator-x boards are also supported. For more information
about Renesas R-Car starter kit please see the
[Renesas product page](https://www.renesas.com/us/en/products/automotive-products/automotive-system-chips-socs/r-car-h3-m3-starter-kit).
The following [optional extras]( {{< ref "extras.md" >}} )
may be of interest.

If you currently don't have access to any of the below supported hardware, the
`amd64` images can be run on a
[virtual machine]( {{< ref "virtualbox.md" >}} ).

The current ARM64 reference device is the [Renesas R-Car M3 Starter Kit Pro (M3SK/m3ulcb)](http://elinux.org/R-Car/Boards/M3SK). See the [Setup guide]( {{< ref "rcar-gen3_setup.md" >}} ).

Alternative ARM64 reference devices are the following:

| Reference                     | Hardware                                                                                        | Status                                          | Comments                                                      |
| ----------------------------- | ----------------------------------------------------------------------------------------------- | ------------------------------------------------| ------------------------------------------------------------- |
| Renesas R-car reference board | [Renesas R-Car H3 Starter Kit Premier (H3SK/h3ulcb)](http://elinux.org/R-Car/Boards/H3SK)       | [Daily tests available](https://qa.apertis.org) | [Setup guide]( {{< ref "rcar-gen3_setup.md" >}} ) [^1]         |
| Renesas R-car reference board | [Renesas R-Car H3 Salvator-X (r8a7795-salvator-x)](http://elinux.org/R-Car/Boards/Salvator-X)   |                                                 | [Setup guide]( {{< ref "rcar-gen3_setup.md" >}} )             |
| Renesas R-car reference board | [Renesas R-Car M3-W Salvator-X (r8a7796-salvator-x)](http://elinux.org/R-Car/Boards/Salvator-X) |                                                 | [Setup guide]( {{< ref "rcar-gen3_setup.md" >}} )             |
| RPi4 reference board          | [Raspberry Pi 4 model B](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/)          | [Daily tests available](https://qa.apertis.org) | [Setup guide]( {{< ref "rpi4_setup.md" >}} )                  |
| RPi4 based custom board       | [Raspberry Pi CM4 CANOPi](https://github.com/boschresearch/kuksa.hardware/)                     |                                                 | [Setup guide]( {{< ref "rpi_cm4_canopi_setup.md" >}} )        |
| i.MX8MN reference board       | [i.MX8MN Variscite Symphony board](https://www.variscite.com/product/evaluation-kits/var-som-mx8m-nano-evaluation-kits/) |                        | [Setup guide]( {{< ref "imx8mn_var_symphony_setup.md" >}} )   |
| i.MX8MN based custom board    | i.MX8MN BSH SMM S2 PRO board                                                                    |                                                 | [Setup guide]( {{< ref "imx8mn_bsh_smm_s2pro_setup.md" >}} )  |
| TI SK-AM62 reference board    | [TI SK-AM62](https://www.ti.com/tool/SK-AM62)                                                   |                                                 | [Setup guide]( {{< ref "sk-am62_setup.md" >}} )               |

[^1]: Tested on H3e-2G(WS3.0) silicon mounted board (`RTP8J779M1ASKB0SK0SA003`)