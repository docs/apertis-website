+++
date = "2020-06-10"
weight = 100

title = "ARM 32-bit Reference Hardware"

aliases = [
    "/reference_hardware/arm32"
]
+++

The recommended ARM 32-bit hardware is the i.MX6 Sabrelite.
Please see its
[setup guide]( {{< ref "imx6q_sabrelite_setup.md" >}} )
for first-time setup.
The following [optional extras]( {{< ref "extras.md" >}} )
may be of interest.

If you currently don't have access to any supported hardware, the `amd64`
images can be run on a
[virtual machine]( {{< ref "virtualbox.md" >}} ).

| Reference                                   | Hardware                                                                                                                                                                             | Status                                           | Comments                                                                    |
| ------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------ | --------------------------------------------------------------------------- |
| ARM iMX6 reference board                    | i.MX6 Sabrelite                                                                                                                                                                      | [Daily tests available](https://qa.apertis.org)  | [Setup guide]( {{< ref "imx6q_sabrelite_setup.md" >}} )                     |
| Recommended: 100 Mbit network switch or hub | Use a network connection that *does not* support gigabit                                                                                                                             |                                                  | Workaround for [reliability issues with this particular hardware when using gigabit](https://community.nxp.com/thread/381119) |
| Recommended: LVDS panel touchscreen         | [HannStar HSD100PXN1-A00-C11](https://eu.mouser.com/ProductDetail/Boundary-Devices/NIT6X_101HANNSTAR/?qs=%2Fha2pyFaduhICqiA62NC348xLf7lkR37nWVKEUqzpogToe6HSSy1X%2FAMJ8uCmShD)       |                                                  |                                                                             |
| Recommended: USB to serial adapter          | [USB\<-\>serial adapter cable](https://www.amazon.com/TRENDnet-Converter-Installation-Universal-TU-S9/dp/B0007T27H8)                                                                 |                                                  | Console cable                                                               |
| Optional: Attached camera                   | [OV5640-based 5MP MIPI attached camera module](https://boundarydevices.com/product/ov5640_camera_imx8m/)                                                                             |                                                  |                                                                             |
