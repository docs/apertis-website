+++
date = "2024-07-09"
weight = 100

title = "Setup Guides"
+++

This section lists setup guides for reference hardware.

{{< section-toc >}}
