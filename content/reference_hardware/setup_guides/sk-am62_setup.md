+++
date = "2024-02-21"
weight = 100

title = "TI SK-AM62 Setup"

aliases = [
    "/reference_hardware/sk-am62_setup"
]
+++

{{% notice note %}}
Support for the SK-AM62 using the process described below has been integrated
into the development branch for v2025 and thus will be available from v2025 and
onwards.
{{% /notice %}}

# Required Items

You should have at least:

- 1 TI SK-AM62 device
- 1 USB-C Power adaptor
- 1 USB to Micro-USB cable
- 1 MicroSD card
- 1 Network cable (optional)

# SK-AM62 Board Setup

The generic Apertis ARM 64-bit image files do not contain a bootloader in order
to remain as board agnostic as possible. We will utilise these by installing
the bootloader in the onboard eMMC's hardware boot partition and configuring to
board to boot from it. The bootloader will scan for bootable installs on both
the MicroSD card and  eMMC at boot, allowing either to be used.

## Attach Serial

The serial console is provided by an onboard serial to USB controller. 

1. Attach the Micro-USB cable to the Micro-USB port labeled `UART`.

2. Connect USB serial to host.

3. Connect terminal emulator on your host.

   Depending on USB serial and operational system you are using the name of
   UART may differ, as well as the terminal emulator. The parameters for your
   terminal emulator should be `115200/8-N-1`.

   For Linux-based systems it is usually `ttyUSB0` and you may use `minicom` or
   `screen` to connect to serial:
   ```
   minicom -b 115200 -o -D /dev/ttyUSB0
   ```
   or
   ```
   screen /dev/ttyUSB0 115200
   ```

![Cable connections to board](/images/sk-am62-cable-connection.jpg)

## Setting Jumpers for SD Card Boot

Configure the jumpers on the SK-AM62 to boot from the SD card as follows:

{{% notice warning %}}
The jumpers are not in a logical order and are understood to vary with board
revisions. Please refer to the documentation for revision of your board. 
{{% /notice %}}

| Jumper | State |
| ------ | ----- |
| B0     | On    |
| B1     | On    |
| B2     | Off   |
| B3     | Off   |
| B4     | Off   |
| B5     | Off   |
| B6     | On    |
| B7     | On    |
| B8     | Off   |
| B9     | On    |
| B10    | On    |
| B11    | Off   |
| B12    | Off   |
| B13    | Off   |
| B14    | Off   |
| B15    | Off   |

On the `E3` revision of the board this will be as shown here:

![State of jumpers for SD Card boot](/images/sk-am62-jumpers-sdcard.jpg)

# Apertis Installation

## Installing Bootloader

1. Download and write the SK-AM62 U-Boot installer (called
   `uboot-<version>-installer-am62-sk.img.gz`) to the MicroSD card and insert
   into the MicroSD card slot.
1. Connect the USB-C power adaptor to the USB port labled `TYPE-C PWR` to power
   up the SK-AM62.
1. Once installation of U-Boot on the SK-AM62 has completed, the following
   message will be displayed:
   ```
   +-------------------------------------------------------------------+
   |                  U-Boot installation complete                     |
   |                                                                   |
   | Please remove the SD Card, set the boot jumpers to boot from eMMC |
   |               and power cycle the board to continue               |
   +-------------------------------------------------------------------+
   ```
1. Power off the board and remove the MicroSD card.

## Setting Jumpers for eMMC HW Boot Partition Boot

Switch the jumpers on the SK-AM62 to boot from the eMMC HW boot partition:

{{% notice warning %}}
The jumpers are not in a logical order and are understood to vary with board
revisions. Please refer to the documentation for revision of your board. 
{{% /notice %}}

| Jumper | State |
| ------ | ----- |
| B0     | On    |
| B1     | On    |
| B2     | Off   |
| B3     | On    |
| B4     | Off   |
| B5     | Off   |
| B6     | On    |
| B7     | On    |
| B8     | Off   |
| B9     | Off   |
| B10    | On    |
| B11    | Off   |
| B12    | Off   |
| B13    | Off   |
| B14    | Off   |
| B15    | Off   |

On the `E3` revision of the board this will be as shown here:

![State of jumpers for eMMC hardware boot partition boot](/images/sk-am62-jumpers-emmc.jpg)

## Installing Rootfs

When configured as described above, the SK-AM62 can utilise the generic ARM
64-bit images such as listed on the [download]( {{< ref "/download.md" >}} )
page.

1. Download the required image and write to the MicroSD card.
1. Insert the card into the MicroSD card slot and power up the board.

If you wish to boot the OS from the eMMC:

1. Boot as described above from the MicroSD card.
1. If the image contains the `bmaptool` utility and the device has an external
   network connection, the image can be directly installed onto the eMMC using
   this tool, for example loading the v2023 OSTree fixedfunction image onto
   the eMMC can be performed with the following command:
   ```
   $ sudo bmaptool copy https://images.apertis.org/release/v2023/v2023.4/arm64/fixedfunction/apertis_ostree_v2023-fixedfunction-arm64-uboot_v2023.4.img.gz /dev/mmcblk0
   ```

   {{% notice tip %}}
   There are other options if `bmaptool` is not present and/or an external
   network connection is not possible:
   - If the image has an external connection `bmaptool` can be installed with
     the following commands:
     ```
     $ sudo sed -i 's/^deb .* target$/& development/' /etc/apt/sources.list
     $ sudo apt update
     ...
     $ sudo apt install bmap-tools
     ```
   - If the image has `bmaptool` installed, but no external network connection
     instead, copy the required image to the running system using `scp` and run:
     ```
     $ sudo bmaptool copy apertis_ostree_v2023-fixedfunction-arm64-uboot_v2023.3.img.gz /dev/mmcblk0
     ```
   - If the image doesn't have bmap tool installed and it's not desirable to
     install it, upload the required image to the running system using `scp` and
     use `dd` instead:
     ```
     $ sudo dd if=apertis_ostree_v2023-fixedfunction-arm64-uboot_v2023.4.img.gz of=/dev/mmcblk0
     ```
   {{% /notice %}}

1. Power down the board and remove the MicroSD card
1. The SK-AM62 should now be able to be powered back up with Apertis running
   from the eMMC.
