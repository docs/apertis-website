+++
date = "2024-04-24"
weight = 100

title = "Maintenance"
+++

The documents provided in this section is targeted towards the Apertis maintainers, covering the tasks required to maintain and develop the core Apertis distribution.

For higher level descriptions of the technology employed in Apertis, please see
the [architecture documents]({{< ref "architecture" >}}). For
documentation targeted at developers and users of Apertis, please see the
[guides]({{< ref "guides" >}}).

{{< section-toc >}}
