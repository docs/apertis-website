+++
date = "2019-12-03"
lastmod = "2021-09-06"
weight = 100

title = "Enabling New Hardware"

aliases = [
    "/old-wiki/Enabling_New_Hardware",
    "/guides/enabling_new_hardware"
]
+++

Apertis is meant as a very flexible baseline to base a variety of
different product and product types. This flexibility not only in the
software selection used, but also in the hardware platforms, boards and
features.

As Apertis aims to cater to a variety of use-cases and hardware
platforms (with each platform providing its own balance of performance
and features), developers are free to pick the hardware platform that
provides the best match to their requirements. As an example, currently
there are two big flavours of images targeted towards devices:

  - HMI Images: The "default" images with a reference
    [HMI]( {{< ref "/glossary.md#hmi" >}} ) implementation. Expects hardware
    features like a 3D capable graphics system, touchscreen, hardware
    video decoding to be utilised.
  - Fixed Function Images: GUI-less images with a minimal footprint (though
    images can be [scaled down
    further]( {{< ref "scaleddownapertisimage.md" >}} ) if that's required).
    Only relies on network connectivity and generally has far lower
    resource demands.

Finally to make it possible to support various boards, it is critical
that the integration of hardware support is done in a very consistent
way using a consistent set of interfaces. This prevents the need for
big, and potentially conflicting, adaptions in the generic parts of the
software stack.

## Adding Hardware Support

Typically the end-goal of adding hardware support is either to:

  - Build new images for it (e.g. a new board)
  - Add support for it to one or more existing images (e.g. add support
    for a previously unsupported USB modem).

In either case the first step is to add the hardware support into the
relevant (Debian) package or introduce new (Debian) packages with
support. For example, for a new modem family support might need to be
added to ofono, but only ofono. However for a new board a selection of
packages and package update will be required (e.g. kernel, bootloader,
firmware, graphics stack).

For more information about the requirements with respect to hardware
enabling see the [Hardware Enablement]( {{< ref "hardware_enablement.md" >}} )
page.

Once the hardware support is available in packages and accepted in the
mainline distribution the next step is to make those available in images
if applicable.

If the additional support doesn't require a new package (e.g. an ofono
plugin for a modem which is included in the ofono package), no further
work needs to done as such a package will already be installed on all
relevant image types.

However, in case the new hardware support does require new packages or
if it's specific to a board or a platform, image integration is the next
step. Apertis takes a two step approach to building images:

  - Generate an OSpack: A root file system containing all generic
    software for a particular image type and hardware architecture (e.g.
    HMI OSpack, containing the HMI stack for the arm architecture).
  - Apply a hardware pack: A combination of hardware specific packages
    (e.g. bootloader, kernel, graphics stack if not generic) and
    configuration metadata (partition layout, bootloader setup
    information etc), that are combined with the OS pack to generate a
    hardware specific image.

If the new hardware support can be used on various boards (e.g. support
for an certain type of USB device/dongle), the package should be added
to the ospack such that it's available for all images of that type. If
however, it's specific to a board then such a packages would be added to
the hwpack (making it only available for that specific boardboard), of
course if the board is completely new for Apertis a new hardware pack
should be created.

More information about image building in can be found on [Image
building]( {{< ref "image_building.md" >}} ).
