+++
date = "2024-06-05"
weight = 100

title = "Licensing"
+++

The documents provided in this section is targeted towards the Apertis developers, which
interested in the tooling related to licensing.

{{< section-toc >}}
