+++
date = "2014-12-10"
weight = 100

title = "Connman Proxy Setup"

aliases = [
    "/old-wiki/Docs/ProxySetup",
    "/guides/connmanproxysetup"
]
+++

The proxy used by a specific interface can be configured for connman on the
command line with:

    connmanctl config <service> --proxy manual <proxy url and port>

Where `<service>` is the name of the interface and `<proxy url and port>` is
the proxy to be used.

You can list services with `connmanctl services`:

    $ connmanctl services
    *AO Wired                ethernet_525400a6d228_cable

So to set the wired ethernet interface from the example above to use a HTTP
proxy on `127.0.0.1` at port `3128` this would be:

    $ connmanctl config ethernet_525400a6d228_cable --proxy manual http://127.0.0.1:3128/

The existing configuration can be seen with `connmanctl services <service>`.
