+++
date = "2018-10-17"
lastmod = "2022-02-01"
weight = 100
toc = true

title = "Apertis Development Environment"

aliases = [
    "/old-developer/latest/appdev-ade.html",
    "/old-developer/v2019/appdev-ade.html",
    "/old-developer/v2020/appdev-ade.html",
    "/old-developer/v2021pre/appdev-ade.html",
    "/old-developer/v2022dev0/appdev-ade.html",
    "/old-wiki/SDK",
    "/guides/ade"
]
+++

Apertis provides tools, pre-installed in the Apertis SDK, for the purpose of
downloading and interacting with sysroots.

These tools are pre-installed in the Apertis SDK. The SDK images shipped by
Apertis are meant to be run in [VirtualBox]( {{< ref "virtualbox.md" >}} ).
They provide a common, reproducible environment to build both platform
packages and application-bundles for Apertis.  The tool provides general manual
pages so you can get command information by using `man ade` on the SDK.

{{% notice info %}}
This page formerly contained information on building and debugging applications
built on top of the legacy Canterbury-based application framework. As part of
the [migration to the next-gen framework]( {{< ref "application-framework.md" >}} ),
this documentation has been migrated to [the Flatpak guide]( {{< ref "flatpak.md" >}} ).
{{% /notice %}}

# Installation of a sysroot

`ade sysroot list` is used to check which sysroots are already installed
on the SDK.  If there's no installed sysroot image, the result would be like the following
message.


    $ ade sysroot list
    No sysroot installed in directory /opt/sysroot/.

`ade sysroot latest` provides the version and downloadable URL of the sysroot image.

    $ ade sysroot latest
    * No distribution specified, defaulting to host distribution
    * No release version specified, defaulting to host release version
    * No architecture specified, defaulting to 'armhf'
    * Checking latest version available for apertis - v2021 (armhf)
    * Retrieved latest version: apertis v2021 - 20210915.0115 (armhf)
    * Download URL: https://images.apertis.org/daily/v2021/20210915.0115/armhf/sysroot/sysroot-apertis-v2021-armhf-20210915.0115.tar.gz

The `ade sysroot install` command will do everything needed to install the
latest sysroot version on your SDK, from downloading to installing without any
manual intervention.  This will default to installing a sysroot for `armhf`,
using the distribution and release of the current SDK, specify using the
command line arguments if this is not what is required (see `ade sysroot
install --help` for more information):

    $ ade sysroot install
    * No distribution specified, defaulting to host distribution
    * No release version specified, defaulting to host release version
    * No architecture specified, defaulting to 'armhf'
    * Installing version apertis v2021 - 20210915.0115 (armhf)
    sysroot.tar.gz |==================================================| 100%

When the installation command has completed its job, the installed status can be checked with `ade sysroot installed`.

    $ ade sysroot installed
    * No distribution specified, defaulting to host distribution
    * No release version specified, defaulting to host release version
    * No architecture specified, defaulting to 'armhf'
    * Retrieved current version: apertis v2021 - 20210915.0115 (armhf)
