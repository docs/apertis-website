+++
title = "Apertis packages checking with lintian"
date = "2024-02-23"
weight = 100
toc = true

aliases = [
    "/guides/apertis-packages-lintian",
]
+++


`lintian` is the Debian package checker. It describes itself as:

> Lintian dissects Debian packages and reports bugs and policy violations. It
 contains automated checks for many aspects of Debian policy as well as some
 checks for common errors.
 This package is useful for all people who want to check Debian packages for
 compliance with Debian policy. Every Debian maintainer should check packages
 with this tool before uploading them to the archive.

Debian packaging can sometimes be not trivial especially because the
[Debian Policy](https://www.debian.org/doc/debian-policy/) is a complex document
which evolves over time. `lintian` is a great tool to help us up to date with
best packaging practices.

Because Apertis is a Debian derivative but has its own identity, we create an
[Apertis profile for lintian](https://gitlab.apertis.org/pkg/lintian/-/tree/apertis/v2024/profiles/apertis)
teaching lintian the Apertis' specificities. For now, this profile avoids lintian
to report tags that are not issues in Apertis. But, it could in the future add
new checks for Apertis.

Most of the packages in Apertis come from Debian without any modification, thus
checking them doesn't have much interest. But, some packages are specific to
Apertis and checking them automatically allow to improve their quality and
technical debt.

# How to enable lintian job for a specific package

To enable the lintian job for a specific package, we only have to create a file
`debian/apertis/lintian` file in the package repository.

```sh
touch debian/apertis/lintian
git add debian/apertis/lintian
git commmit -m "Add debian/apertis/lintian to enable lintian for my package"
```

The `ci-package-builder` pipeline will check the presence of this file and will
add the lintian job if this file is present. If lintian emits any `warning` or
`error` tag then the job will fail.

# Add lintian overrides
Occasionally, lintian wrongly emits tags which can be *false positives* or a
*lintian bug*. In this case, it is possible to teach lintian not to report them
by adding lintian overrides in the source package. Please refer to the lintian
documentation (available at `/usr/share/doc/lintian/lintian.html` after having
installed `lintian`) or to the [Debian Maintainers' Guide](https://www.debian.org/doc/manuals/maint-guide/dother.en.html#lintian).

# Behind the scene
The `ci-package-builder` will build a source package on `GitLab`, then this source
package will be sent to `OBS` where binary packages will be built. Once, binary
packages are built, `GitLab` will retrieve them and will run lintian on `*.deb`
and `*.dsc` files like:

```sh
lintian --verbose --info --color always --fail-on error,warning --profile apertis --allow-root *.deb
```

# References
- [Apertis lintian package](https://gitlab.apertis.org/pkg/lintian)
- [Apertis profile for lintian](https://gitlab.apertis.org/pkg/lintian/-/tree/apertis/v2024/profiles/apertis)
- [List of all lintian tags](https://udd.debian.org/lintian-tag.cgi)
- [Debian Policy](https://www.debian.org/doc/debian-policy/)
