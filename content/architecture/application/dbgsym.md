+++
date = "2016-09-05"
weight = 100

title = "Debug Symbol Packages"

aliases = [
    "/old-wiki/Dbgsym",
    "/architecture/dbgsym"
]
+++

Packages automatically generate debug symbol packages at build time, the
packages have the package name extension `-dbgsym`.

# Infrastructure

To be able to benefit from `dbgsym` packages, the infrastructure must be ready
for them, the requirements are:

- `reprepro`: must support `dbgsym` since `4.17.0`
- `debhelper`: with `dbgsym` support since `9.20160114`
- `dpkg-dev`: support for `dbgsym` since `1.18.2~`

When building packages with those tools, automatic debug symbols will be
generated for all built packages.

`dbgsym` packages are supported since the Apertis `16.06` release, however not
all packages were rebuilt, therefore there are packages without `dbgsym`
packages. For the `16.09` release a full rebuild of the main archives was
performed to ensure all packages have a corresponding `dbgsym` package.

# How to use dbgsym packages

Usually, a package name with the `-dbgsym` extension, from Apertis
repositories, needs to be installed in the target runtime system, then your
system is able to resolve debug symbols for debugging purposes.

As reference you might read Debian documentation on [how to get a
backtrace](https://wiki.debian.org/HowToGetABacktrace), but please make sure to
use Apertis repositories to fetch the `dbgsym` packages.

# dbg packages are deprecated

Before `dbgsym` packages, package maintainers had to manually create their
`-dbg` packages listed in the `debian/control` file, these packages were
mirrored in every mirror and every suite, wasting lots of storage space. This
is not relevant for Apertis, but it is very relevant for Debian (Apertis parent
distribution). With `dbgsym` packages being produced, there is no need to add
any new stanzas to `debian/control`.

If you want to transition from a former `-dbg` package to a `-dbgsym` package
you might want to look into the
[dh_strip](http://manpages.ubuntu.com/manpages/xenial/en/man1/dh_strip.1.html)'s
`--dbgsym-migration=pkgname-dbg (<< currentversion~)` switch and drop `-dbg`
from `debian/control` file.
