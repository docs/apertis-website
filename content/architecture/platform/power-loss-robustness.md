+++
date = "2014-12-20"
weight = 100

title = "Power Loss Robustness"

aliases = [
    "/old-wiki/Docs/power-loss-robustness",
    "/architecture/power-loss-robustness"
]
+++

In order to test that Apertis is capable of handling power loss, the tests from
from GLib test suite to ensure that the robustness features built into these
libraries can be verified to be working.

## GLib/GIO

The [GIO](https://developer.gnome.org/gio/) library within
[GLib](https://developer.gnome.org/glib/) is the standard way to do file
handling, I/O, etc. It also has a set of functional tests for all the features
provided by the library. Out of these, the subset of GIO tests that deal with
local I/O have been integrated into
[the image testing]( {{< ref "/qa/test_cases/glib-gio-fs.md" >}} ).
