+++
title = "Software Bill of Materials"

date = "2024-06-28"
weight = 100
toc = true
+++

# Software Bill of Materials

Software Bill of Materials (SBOM) is an essential part in the supply chain
management, especially for complex open source projects like Apertis. Indeed,
Apertis combines components from [different sources]({{< ref "upstreaming.md" >}})
and with [different licenses]({{< ref "license-expectations.md" >}}). This
diversity could lead to license compliance failures and/or security failures
without a strong automatic listing process. SBOM allows to overcome this situation
by building an inventory of all components used in a product. For these traceability
purposes, every Apertis images come with several SBOM reports.

## Licenses SBOM

To ensure a product is compliant its components' licenses, a list of
**all licenses** must be generated. The whole process to generate this licenses
report is described in the document
[Automated License Compliance](https://www.apertis.org/concepts/automated-license-compliance/).
Roughly, it relies on the Debian's tool `scan-copyrights` to detect licenses and
copyright of each files used. The result is integrated into binary packages. Then,
when building an image, a script scans all packages used to assemble all licenses
and copyright used in the image.

For instance, the licenses SBOM report for the image
`apertis_v2024-fixedfunction-amd64-uefi_v2024.1.img` is available here
[v2024.1 licenses SBOM](https://images.apertis.org/release/v2024/v2024.1/amd64/fixedfunction/apertis_v2024-fixedfunction-amd64-uefi_v2024.1.img.licenses.gz).

Moreover, [OSS Review Toolkit](https://github.com/oss-review-toolkit/ort) is used
to generate supplementary reports in [various formats](https://oss-review-toolkit.org/ort/docs/tools/reporter)
(`YAML`, `PDF`, [CycloneDX](https://cyclonedx.org/), [SPDX](https://spdx.dev/use/specifications/),
and `WebApp`) allowing an easy review of the licenses used by Apertis images.
Among the generated reports, there are human friendly ones like the dynamic
WebApp report which includes a summary of detected license with statistics and
diagrams. For instance, the ORT's reports for the image
`apertis_v2024-fixedfunction-amd64-uefi_v2024.1.img` are available here
[v2024.1 ORT reports](https://images.apertis.org/release/v2024/v2024.1/amd64/fixedfunction/apertis_v2024-fixedfunction-amd64-uefi_v2024.1.img.ort-reports.tar.gz).

This makes Apertis compliant with the [OpenChain ISO/IEC 5230:2020](https://openchainproject.org/license-compliance)
International Standard.

## Security SBOM

Tracking vulnerabilities of a product during its whole lifetime can be challenging,
that is where a SBOM can be of great help.
For a given image, the Apertis security SBOM lists all installed packages and
their version. In addition to that, for every package, all packages (with their
version) used as build dependencies are also listed.
This SBOM allows to easily track which package might need to be rebuilt in case
of a vulnerability was found in its dependency chain.

For instance, the ORT's reports for the image
`apertis_v2024-fixedfunction-amd64-uefi_v2024.1.img` are available here
[v2024.1 security SBOM](https://images.apertis.org/release/v2024/v2024.1/amd64/fixedfunction/apertis_v2024-fixedfunction-amd64-uefi_v2024.1.img.buildeps)

This information helps to easily track CVEs that might potentially affect Apertis
packages.
