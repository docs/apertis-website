+++
title = "Change History"
weight = 100
toc = true
date = "2024-01-24"

aliases = [
    "/architecture/change-history",
]
+++

Whenever daily builds of Apertis images are run, a set of changes from the
release's previous daily images is saved as a JSON file to
`https://images.apertis.org/daily/RELEASE/BUILD/meta/changes-since-PREVIOUS.json`.

# Format

The format of the changes JSON file is:

```javascript
{
  // Array array of images and their changes.
  "images": [
    {
      // Relative path to the previous version of the image, without the file extension.
      // Omitted if the image is new / there is no previous version.
      "previous": "20240124.0017/arm64/fixedfunction/ospack_v2024dev1-arm64-fixedfunction_20240124.0017",
      // Relative path to the current version of the image, without the file extension.
      // Omitted if the image was removed / there is no current version.
      "current": "20240124.1927/arm64/fixedfunction/ospack_v2025dev1-arm64-fixedfunction_20240124.1927",
      // Map of packages to their changes.
      "packages": {
        "libxyz": {
          // Previous version of the package.
          // Omitted if it is newly added / there is no previous version.
          "previous": {
            "version": "1",
            "source": "xyz",
            "source_version": "1",
          },
          // Current version of the package.
          // Omitted if it is now removed / there is no current version.
          "current": {
            "version": "3",
            "source": "xyz",
            "source_version": "3"
          }
        }
      },
      // Map of source package names (from the `source` fields above) to a subset
      // of their changelog entries, guaranteed to span all the versions from
      // immediately after the oldest "previous" version, up to and including the
      // newest "current" version.
      "source_changelogs": {
        "xyz": [
          {
            "author": "John Doe <john.doe@test.com>",
            "timestamp": "2023-03-28T12:47:31+00:00",
            "version": "3",
            "changes": [
              "One change line.",
              "Another change line."
            ]
          },
          {
            "author": "John Doe <john.doe@test.com>",
            "timestamp": "2022-11-11T09:28:15+01:00",
            "version": "2",
            "changes": [
              "One change line.",
              {
                "change": "An individual change by another person.",
                "author": "Jane Doe"
              }
            ]
          }
        ]
      }
    }
  ],
  // An array of the recipes used to build these images and their changes.
  "recipes": [
    {
      "url": "https://gitlab.apertis.org/infrastructure/apertis-image-recipes",
      // The commit used to build the previous set of images.
      "previous": "199b8f3a8c4ef555c7d0a24a2bf2a609687bbc0b",
      // The commit used to build the current set of images.
      "current": "c099ef790301d0c019f5bf756093f3e486273bd7",
      // A list of all the commits after "previous", up to and including "current".
      "log": [
        {
          "id": "3f82f0329bd481af4204a145d313c10d3d872a5c",
          "author": "John Doe <john.doe@test.com>",
          "message": "Commit message.",
          "author_timestamp": "2023-02-27T11:50:59+00:00",
          "commit_timestamp": "2023-02-27T11:50:59+00:00"
        },
        {
          "id": "c099ef790301d0c019f5bf756093f3e486273bd7",
          "author": "John Doe <john.doe@test.com>",
          "message": "Commit message.\n\nSecond line.",
          "author_timestamp": "2023-03-05T19:38:31-03:00",
          "commit_timestamp": "2023-03-07T13:51:47-03:00"
        }
  ]
}
```
