+++
date = "2024-06-05"
weight = 100

title = "Platform"
+++

The documents provided in this section is targeted towards the Apertis developers, which are interested in building their own platform on top of Apertis.

{{< section-toc >}}
