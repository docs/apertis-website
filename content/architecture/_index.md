+++
date = "2020-10-01"
weight = 100

title = "Architecture"
+++

This section provides documentation covering the implementation goals and strategies taken to enable features or functionality in Apertis.

For more detailed guidance on implementing or using specific functionality, please refer to the [guides]({{< ref "guides" >}}).

{{< section-toc >}}
