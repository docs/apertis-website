+++
date = "2022-07-21"
weight = 100

title = "Submitting issues"

+++

Apertis uses [apertis-issues] as the main point to report issues such as bugs or enhancements. Downstream distributions
can use use this repository to track Apertis specific issues. On the other hand, bugs or enhancements in downstream distributions
should follow their own workflow, as all the issues in [apertis-issues] should be public and reproducible in Apertis.

The following section highlight some recommendations on reporting issues using the [apertis-issues] board.

## Workflow

- Go to [apertis-issues] and check if your issue is already listed in the open issues
- Also check the list of closed issues since the problem might be already solved in a newer release, in which case using the issue as reference will be valuable
- If your concern is still valid, [create a new issue](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/new)
- Provide a good **title** for the issue to summarize the problem
- Leave **type** as **issue** as it is the only supported type
- Fill out all the fields in the form in order to provide the QA team as much information as possible
- QA team will triage the newly created issue with a priority and according to it a developer will be assigned
- QA team will update labels as needed to improve the management of the issue
- Different views of the issues are available through Gitlab boards
  - [Priority board](https://gitlab.apertis.org/infrastructure/apertis-issues/-/boards/30)
  - [Progress board](https://gitlab.apertis.org/infrastructure/apertis-issues/-/boards/28)
- As part of the process of the QA team, a task in the internal Phabricator is created for management purposes only and a link
is appended to the original issue.

[apertis-issues]: https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues

See also screenshots below for a reference when reporting new issues.

The following screenshot shows the bug list, the button in the top right corner **New issue** allows users to create a new issue.

![](/images/issues_list.png)

<br>
<br>

Upon pressing the **New issue** button the following form shows up to fill the information about the bug.

![](/images/issues_form_1.png)

<br>
<br>

Finally, at the botton of the page the **Create issue** button can be used to submit the new issue.
![](/images/issues_form_2.png)
