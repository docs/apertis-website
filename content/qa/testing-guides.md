+++
date = "2024-08-30"
weight = 100
toc = true

title = "Manual testing guides"

+++

Apertis runs both automated and manual tests on their images to ensure the quality. The manual
tests are used when it is very difficult to implement an automated tests due to the interactions
needed and the nature of the checks. Examples can be testing video or sound output, or interaction
with mobiles.

To help QA this guides provides reference to adjust configuration used in several according to the
setup.

## Flatpak tests

While testing flatpak it is required to add repositories or install/uninstall applications. It is
important to note that this activities can be done in the user context (`--user`) or system context,
but for the last one admin privileges are required.

Add Flathub repo:
```
flatpak remote-add --user --no-sign-verify --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

Add Apertis Flatpak repo:
```
flatpak remote-add --user apertis https://images.apertis.org/flatpak/repo/apertis.flatpakrepo
```

Install an application:
```
flatpak install --user apertis org.apertis.hmi.totem
```

## Video setup

Some boards like [iMX6 Sabrelite]({{<ref arm32.md >}}) have several video output available, in which
case can convenient to change the default one.

As an example, if the board has LVDS and HDMI output and to make sure the HDMI output is used by the compositor:

```
$ cat /etc/xdg/weston/agl-compositor.ini 
[shell-client]
command=/usr/bin/maynard

[core]
require-input=false

[output]
name=LVDS-1
mode=off
```

## Audio setup

Boards usually come with different audio outputs. In order to configure the desired one, first list
the ones available. For the examples bellow, the [iMX6 Sabrelite]({{<ref arm32.md >}}) has been used.

```
$ pactl list short sinks
41	control.endpoint.phone	PipeWire	float32le 2ch 48000Hz	SUSPENDED
42	control.endpoint.new_email	PipeWire	float32le 2ch 48000Hz	SUSPENDED
43	control.endpoint.gps	PipeWire	float32le 2ch 48000Hz	SUSPENDED
44	control.endpoint.traffic_info	PipeWire	float32le 2ch 48000Hz	SUSPENDED
45	control.endpoint.ringtone	PipeWire	float32le 2ch 48000Hz	SUSPENDED
46	control.endpoint.multimedia	PipeWire	float32le 2ch 48000Hz	SUSPENDED
74	alsa_output.platform-120000.hdmi.stereo-fallback	PipeWire	s24-32le 2ch 48000Hz	SUSPENDED
75	alsa_output.platform-sound.stereo-fallback	PipeWire	s16le 2ch 48000Hz	SUSPENDED
```

If in doubt, the following command gives more information to identify the desired sink:
```
$ pactl list cards
```

After that you it is possible to set the default sink and port by:

```
$ pactl set-default-sink alsa_output.platform-sound.stereo-fallback

$ pactl set-sink-port alsa_output.platform-sound.stereo-fallback analog-output-headphones
```

Also, make sure the sink is unmute:

```
$ pactl set-sink-mute alsa_output.platform-sound.stereo-fallback false
```

In a similar way, it is possible to set the HDMI as default sink:

```
$ pactl set-default-sink alsa_output.platform-120000.hdmi.stereo-fallback
```

## U-boot

One tricky component to test is u-boot, as it cannot be easily tested in LAVA,
as it is tight to the LAVA lab setup. For this reason, when upgrading u-boot
a strict manual test is required to ensure the the functionality it provides
works as expected.

U-boot is responsible for the early boot process and preparing the environment
to load Linux kernel. Also in the OSTree context, it is responsible of checking
the bootcount and provide the rollback functionality if the bootcount is exceeded.

To properly test u-boot the following steps should be covered:
- Test APT image on all reference boards to ensure Linux can be booted
- Test OSTree image on all reference boards, to ensure Linux can be booted
- Test OSTree image rollback on all reference boards, to ensure Linux can be rolled back

The basic test on both APT and OStree image is to make sure that the system boots
without issues by following the steps in [Sanity Test](https://qa.apertis.org/testcases/v2024/sanity-check-manual.html).

Testing the OSTree rollback mechanism is a bit more complex. It requires to test two aspects:
- Bootcount increment
- Rollback on bootcount limit exceeded

The easiest way to test this is to use the testcase
[AUM Rollback Bootcount](https://qa.apertis.org/testcases/v2024/aum-rollback-bootcount.html)
which covers a rollback scenario.
