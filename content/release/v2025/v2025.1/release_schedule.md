+++
date = "2025-03-05"
weight = 100

title = "v2025.1 Release schedule"

aliases = [
    "/release/v2025.1/release_schedule",
]
+++

The v2025.1 release cycle is scheduled to start in January 2025.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2025-04-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2025-05-07        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2025-05-14        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2025-05-21        |
| RC testing                                                                                               | 2025-05-22..06-04 |
| v2025.1 release                                                                                          | 2025-06-05        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
