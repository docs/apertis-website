+++
date = "2024-09-12"
weight = 100

title = "v2025pre Release schedule"

aliases = [
    "/release/v2025pre/release_schedule",
]
+++

The v2025pre release cycle is scheduled to start in October 2024.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2024-10-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2024-11-06        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2024-11-13        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2024-11-20        |
| RC testing                                                                                               | 2024-11-21..12-04 |
| v2025pre release                                                                                        | 2024-12-05        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
