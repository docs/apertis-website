+++
date = "2024-12-04"
weight = 100

title = "v2025pre Release Notes"

aliases = [
    "/release/v2025pre/releasenotes",
]
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2025pre** is the preview release of the Apertis
v2025 stable release flow that will lead to the LTS **Apertis v2025.0**
release in March 2025.

This Apertis release is built on top of Debian Bookworm along with several customizations
and it ships the latest Linux kernel 6.11.x series. Later releases in the v2025 channel
will be tracking newer kernel versions up to the next LTS as well as Debian Bookworm
Stable Channel.

Test results for the v2025pre release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2025pre/20241120.0018/apt)
  - [OSTree images](https://qa.apertis.org/report/v2025pre/20241120.0018/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2025pre/20241120.0018/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2025pre/20241120.0018/lxc)

## Release flow

  - 2023 Q4: v2025dev0
  - 2024 Q1: v2025dev1
  - 2024 Q2: v2025dev2
  - 2024 Q3: v2025dev3
  - 2024 Q4: **v2025pre**
  - 2025 Q1: v2025.0
  - 2025 Q2: v2025.1
  - 2025 Q3: v2025.2
  - 2025 Q4: v2025.3
  - 2026 Q1: v2025.4
  - 2026 Q2: v2025.5
  - 2026 Q3: v2025.6
  - 2026 Q4: v2025.7

### Release downloads

| [Apertis v2025pre.0 images](https://images.apertis.org/release/v2025pre/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2025pre/v2025pre.0/amd64/fixedfunction/apertis_ostree_v2025pre-fixedfunction-amd64-uefi_v2025pre.0.img.gz) | [hmi](https://images.apertis.org/release/v2025pre/v2025pre.0/amd64/hmi/apertis_ostree_v2025pre-hmi-amd64-uefi_v2025pre.0.img.gz) | [base SDK](https://images.apertis.org/release/v2025pre/v2025pre.0/amd64/basesdk/apertis_v2025pre-basesdk-amd64-sdk_v2025pre.0.ova) | [SDK](https://images.apertis.org/release/v2025pre/v2025pre.0/amd64/sdk/apertis_v2025pre-sdk-amd64-sdk_v2025pre.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2025pre/v2025pre.0/armhf/fixedfunction/apertis_ostree_v2025pre-fixedfunction-armhf-uboot_v2025pre.0.img.gz) | [hmi](https://images.apertis.org/release/v2025pre/v2025pre.0/armhf/hmi/apertis_ostree_v2025pre-hmi-armhf-uboot_v2025pre.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2025pre/v2025pre.0/arm64/fixedfunction/apertis_ostree_v2025pre-fixedfunction-arm64-uboot_v2025pre.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2025pre/v2025pre.0/arm64/fixedfunction/apertis_ostree_v2025pre-fixedfunction-arm64-rpi64_v2025pre.0.img.gz) | [hmi](https://images.apertis.org/release/v2025pre/v2025pre.0/arm64/hmi/apertis_ostree_v2025pre-hmi-arm64-rpi64_v2025pre.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "virtualbox.md" >}} ).

#### Apertis v2025pre package list

The full list of packages available from the v2025pre APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2025pre](https://infrastructure.pages.apertis.org/dashboard/tsv/v2025pre.tsv)

#### Apertis v2025pre repositories

    deb https://repositories.apertis.org/apertis/ v2025pre target development sdk non-free

## New features

### Support for NXP LS1028ARDB board

Apertis now provides images for the NXP LS1028ARDB allowing developers building products
using this platform to have a good starting point. The fixed function images is available
in the standard [Apertis download site](https://images.apertis.org/release/v2025pre/v2025pre.0rc1/arm64/fixedfunction/apertis_v2025pre-fixedfunction-arm64-layerscape_v2025pre.0.img.gz)

## Build and integration

### Include builddeps reports

Following the idea of provide more resources for Software Build of Materials, now Apertis provides
a report for the build dependencies of every package in the target images, including its version.
This information helps to easily track CVEs that might potentially affect Apertis packages.

### Support for tests on MRs

Following the guidelines described in the [Apertis test strategy]({{< ref "test-strategy.md" >}})
this release provides support for running automated tests on LAVA in the context of a MR.
With this feature, packages changes can be validated by running integration tests
before landing them to main branches.

It is currently recommended that only packages where potential regressions
may have a high-impact on the system stability have this feature enabled, given the extra
resources required to run the tests.

As before, all available integration tests are also run regularly on daily image builds.

### Support for package testing

This release includes a new way for developers to includes tests to run on a Merge Request.
The [package testing in LAVA]({{< ref "apertis-packages-testing.md" >}}) allows writing tests a part of the
standard packing metadata, which are used by our GitLab infrastructure to run LAVA tests to
ensure the proposed changes do not introduce any kind of regression in the supported hardware.

### Test on MR for linux

Continuing with the trend on adding test on MR, a set of custom tests have been implemented
for the linux kernel. As this is a very special package with delta respect to Debian
additional checks are now performed, to ensure that changes proposed by developers don't
cause regression and simplifying the review and test process.

### Support for image change history

Apertis now provides [Change History Generator](https://gitlab.apertis.org/infrastructure/change-history-generator/)
a tool to generate a changelog between images. Thanks to this tool, as part of
[daily builds](https://images.apertis.org/daily/v2024/) a
[changelog](https://images.apertis.org/release/v2024/v2024.0/amd64/fixedfunction/ospack_v2024-amd64-fixedfunction_v2024.0.changelogs.tar.gz)
is created which helps developers to track changes across different images.

### Folding and branching pipeline improvements

The folding and branching process has been moved to Gitlab CI, but the work continues to
make these pipelines more developer and user friendly. With this idea in mind the code has
been refactored to improve consistency and make it easier to maintain and debug. From the
user perspective, the new version provides more hints to and checks to avoid mistakes and
improve debug information.

### Dashboard delta tracking

Continuing with the effort on tracking delta from upstream,
[Apertis dashboard](https://infrastructure.pages.apertis.org/dashboard/)
provides more refined information about the root cause of the delta. Based on this information
the Apertis team continuously [upstreams changes](https://www.apertis.org/policies/upstreaming/)
to benefit the community.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

No known breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025pre-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025pre-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025pre-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025pre-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025pre-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025pre-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #615](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/615) aum-offline-upgrade-branch: test failed
- [Issue #636](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/636) WiFi connection does not bring up after reboot
- [Issue #641](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/641) aum-ota-rollback-blacklist: test failed
- [Issue #642](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/642) aum-rollback-blacklist: test failed
- [Issue #671](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/671) Package rust-coreutils does not provide external reference files
- [Issue #695](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/695) aum-rollback-bootcount: error message displayed during the test
- [Issue #696](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/696) aum-out-of-space: test failed

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #9](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/9) System users are shipped in /usr/etc/passwd instead of /lib/passwd
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #49](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/49) "firmware: failed to load" logs seen during boot
- [Issue #66](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/66) Some binaries in the toolchain tarball are huge
- [Issue #377](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/377) import-debian-package issues during package import
- [Issue #396](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/396) Pipeline to build package fails during build-source for texlive-extra
- [Issue #405](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/405) traprain: test failed
- [Issue #413](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/413) obs-runner: Disabled architectures on a OBS package get re-enabled when CI creates a branch
- [Issue #448](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/448) ade-commands: test failed
- [Issue #459](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/459) tiny-container-system-device-sharing: test failed
- [Issue #462](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/462) agl-compositor mute functionality not working
- [Issue #486](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/486) "webkit2gtk-ac-3d-rendering" fails in "ARMHF".
- [Issue #490](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/490) gpgv (sequioa) crashes when added debian archives
- [Issue #528](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/528) OBS runner creates conflicts in the origin repo, then waits forever for them
- [Issue #597](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/597) obs runner is confused by multple OBS repos
- [Issue #614](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/614) sdk-cross-compilation: test failed
- [Issue #633](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/633) dashboard: issues just after a branching
- [Issue #637](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/637) Improve the repo/pipeline used to test the lintian job
- [Issue #638](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/638) Improve the repos/pipelines used to test the abi-checker job
- [Issue #639](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/639) apertis-dev depends on eatmydata that was dropped during the rebase on Bookworm
- [Issue #643](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/643) Improve the tool import-debian-package to trigger the generation of debian/apertis/copyright on an initial import.
- [Issue #650](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/650) Investigate upstreamble solution for rollback support when using u-boot with bootstd
- [Issue #652](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/652) sdk-import-debian-package: test failed
- [Issue #656](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/656) Improve apertis-pkg-* tools (from apertis-dev-tools) to use python-gitlab instead of relying on urllib
- [Issue #659](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/659) Remove "hmi" repository from website documentation
- [Issue #664](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/664) Improve workflow for importing packages
- [Issue #665](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/665) Write a python tool to generate apertis-oslist.json (for rpi-imager)
- [Issue #675](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/675) pkg/debugpy: investigate tests failure at build time
- [Issue #678](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/678) Test pipeline of ci-flatdeb-builder triggers too many (5) MR pipelines

### Low
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #607](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/607) Pipeline for sample application helloworld-https-client fails

