+++
date = "2024-06-10"
weight = 100

title = "v2025dev3 Release schedule"

aliases = [
    "/release/v2025dev3/release_schedule",
]
+++

The v2025dev3 release cycle is scheduled to start in July 2024.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2024-07-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2024-08-14        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2024-08-21        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2024-08-28        |
| RC testing                                                                                               | 2024-08-29..09-11 |
| v2025dev3 release                                                                                        | 2024-09-12        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
