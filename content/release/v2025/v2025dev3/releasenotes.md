+++
date = "2024-09-12"
weight = 100

title = "v2025dev3 Release Notes"

aliases = [
    "/release/v2025dev3/releasenotes",
]
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2025dev3** is the fourth development release of the Apertis
v2025 stable release flow that will lead to the LTS **Apertis v2025.0**
release in March 2025.

This Apertis release is built on top of Debian Bookworm along with several customizations
and it ships the latest Linux kernel 6.9.x series. Later releases in the v2025 channel
will be tracking newer kernel versions up to the next LTS as well as Debian Bookworm
Stable Channel.

Test results for the v2025dev3 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2025dev3/20240828.0018/apt)
  - [OSTree images](https://qa.apertis.org/report/v2025dev3/20240828.0018/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2025dev3/20240828.0018/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2025dev3/20240828.0018/lxc)

## Release flow

  - 2023 Q4: v2025dev0
  - 2024 Q1: v2025dev1
  - 2024 Q2: v2025dev2
  - 2024 Q3: **v2025dev3**
  - 2024 Q4: v2025pre
  - 2025 Q1: v2025.0
  - 2025 Q2: v2025.1
  - 2025 Q3: v2025.2
  - 2025 Q4: v2025.3
  - 2026 Q1: v2025.4
  - 2026 Q2: v2025.5
  - 2026 Q3: v2025.6
  - 2026 Q4: v2025.7

### Release downloads

| [Apertis v2025dev3.0 images](https://images.apertis.org/release/v2025dev3/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2025dev3/v2025dev3.0/amd64/fixedfunction/apertis_ostree_v2025dev3-fixedfunction-amd64-uefi_v2025dev3.0.img.gz) | [hmi](https://images.apertis.org/release/v2025dev3/v2025dev3.0/amd64/hmi/apertis_ostree_v2025dev3-hmi-amd64-uefi_v2025dev3.0.img.gz) | [base SDK](https://images.apertis.org/release/v2025dev3/v2025dev3.0/amd64/basesdk/apertis_v2025dev3-basesdk-amd64-sdk_v2025dev3.0.ova) | [SDK](https://images.apertis.org/release/v2025dev3/v2025dev3.0/amd64/sdk/apertis_v2025dev3-sdk-amd64-sdk_v2025dev3.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2025dev3/v2025dev3.0/armhf/fixedfunction/apertis_ostree_v2025dev3-fixedfunction-armhf-uboot_v2025dev3.0.img.gz) | [hmi](https://images.apertis.org/release/v2025dev3/v2025dev3.0/armhf/hmi/apertis_ostree_v2025dev3-hmi-armhf-uboot_v2025dev3.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2025dev3/v2025dev3.0/arm64/fixedfunction/apertis_ostree_v2025dev3-fixedfunction-arm64-uboot_v2025dev3.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2025dev3/v2025dev3.0/arm64/fixedfunction/apertis_ostree_v2025dev3-fixedfunction-arm64-rpi64_v2025dev3.0.img.gz) | [hmi](https://images.apertis.org/release/v2025dev3/v2025dev3.0/arm64/hmi/apertis_ostree_v2025dev3-hmi-arm64-rpi64_v2025dev3.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "virtualbox.md" >}} ).

#### Apertis v2025dev3 package list

The full list of packages available from the v2025dev3 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2025dev3](https://infrastructure.pages.apertis.org/dashboard/tsv/v2025dev3.tsv)

#### Apertis v2025dev3 repositories

    deb https://repositories.apertis.org/apertis/ v2025dev3 target development sdk non-free

## New features

### Support for NXP LS1028ARDB board

Apertis now provides images for the NXP LS1028ARDB allowing developers building products
using this platform to have a good starting point. The fixed function images is available
in the standard [Apertis download site](https://images.apertis.org/release/v2025dev3/v2025dev3.0rc1/arm64/fixedfunction/apertis_v2025dev3-fixedfunction-arm64-layerscape_v2025dev3.0.img.gz)

## Build and integration

### Folding and branching pipeline improvements

The folding and branching process has been moved to Gitlab CI, but the work continues to
make these pipelines more developer and user friendly. With this idea in mind the code has
been refactored to improve consistency and make it easier to maintain and debug. From the
user perspective, the new version provides more hints to and checks to avoid mistakes and
improve debug information.

### Dashboard delta tracking

Continuing with the effort on tracking delta from upstream,
[Apertis dashboard](https://infrastructure.pages.apertis.org/dashboard/)
provides more refined information about the root cause of the delta. Based on this information
the Apertis team continuously [upstreams changes](https://www.apertis.org/policies/upstreaming/)
to benefit the community.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

No known breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025dev3-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025dev3-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025dev3-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025dev3-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025dev3-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025dev3-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #419](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/419) High level tracking of AUM issues
- [Issue #431](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/431) Package rust-coreutils does not provide license mapping information
- [Issue #615](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/615) aum-offline-upgrade-branch: test failed
- [Issue #616](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/616) polkit-parsing: test failed
- [Issue #636](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/636) WiFi connection does not bring up after reboot

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #273](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/273) Debos build output hidden on GitLab job view can only be seen in the raw log file
- [Issue #322](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/322) wrap-and-sort fails to parse some debian/control.in files
- [Issue #330](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/330) datefudge: 64-bit time_t support on 32-bit archs
- [Issue #339](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/339) Up Squared 6000 board fails to boot randomly in LAVA
- [Issue #377](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/377) import-debian-package issues during package import
- [Issue #396](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/396) Pipeline to build package fails during build-source for texlive-extra
- [Issue #405](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/405) traprain: test failed
- [Issue #413](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/413) obs-runner: Disabled architectures on a OBS package get re-enabled when CI creates a branch
- [Issue #448](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/448) ade-commands: test failed
- [Issue #459](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/459) tiny-container-system-device-sharing: test failed
- [Issue #485](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/485) AUM tests fail on v2024 and v2025dev* armhf
- [Issue #486](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/486) "webkit2gtk-ac-3d-rendering" fails in "ARMHF".
- [Issue #488](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/488) Flashing an HMI image on the eMMC of R-car H3e-2G board leads to a kernel panic.
- [Issue #490](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/490) gpgv (sequioa) crashes when added debian archives
- [Issue #491](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/491) AM62x: Do not install firmware prerequisites in image for boot firmware generation
- [Issue #528](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/528) OBS runner creates conflicts in the origin repo, then waits forever for them
- [Issue #537](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/537) Update some apertis gitlab-ci pipeline to use a bookworm image instead of a bullseye/buster image
- [Issue #539](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/539) Create automatic sdk tests in apertis-test-cases for abi-checker job
- [Issue #541](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/541) QA Report App is not able to login
- [Issue #554](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/554) import-debian-package doesn't trigger the pipeline adding a debian/apertis/copyright file
- [Issue #578](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/578) qa report application creates multiple entries per issue
- [Issue #597](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/597) obs runner is confused by multple OBS repos
- [Issue #602](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/602) lintian: the child pipeline lintian-errors is always triggered on the default branch instead of the same branch as the parent job
- [Issue #603](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/603) AUM upgrade branch fails on amd64 on all releases
- [Issue #611](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/611) RPi4 is unable to boot linux 6.8
- [Issue #614](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/614) sdk-cross-compilation: test failed

### Low
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #65](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/65) Terminal at xfce desktop corrupted after debos call

