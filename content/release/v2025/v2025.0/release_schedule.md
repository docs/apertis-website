+++
date = "2024-12-04"
weight = 100

title = "v2025.0 Release schedule"

aliases = [
    "/release/v2025.0/release_schedule",
]
+++

The v2025.0 release cycle is scheduled to start in January 2025.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2025-01-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2025-02-05        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2025-02-12        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2025-02-19        |
| RC testing                                                                                               | 2025-02-20..03-05 |
| v2025.0 release                                                                                        | 2025-03-06        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
