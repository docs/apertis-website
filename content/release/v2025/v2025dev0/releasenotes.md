+++
date = "2023-12-14"
weight = 100

title = "v2025dev0 Release Notes"

aliases = [
    "/release/v2025dev0/releasenotes",
]
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2025dev0** is the first development release of the Apertis
v2025 stable release flow that will lead to the LTS **Apertis v2025.0**
release in March 2025.

This Apertis release is built on top of Debian Bookworm along with several customizations
and it ships the latest Linux kernel 6.6.x series. Later releases in the v2025 channel
will be tracking newer kernel versions up to the next LTS as well as Debian Bookworm
Stable Channel.

Test results for the v2025dev0 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2025dev0/20231129.1807/apt)
  - [OSTree images](https://qa.apertis.org/report/v2025dev0/20231129.1807/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2025dev0/20231129.1807/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2025dev0/20231129.1807/lxc)

## Release flow

  - 2023 Q4: **v2025dev0**
  - 2024 Q1: v2025dev1
  - 2024 Q2: v2025dev2
  - 2024 Q3: v2025dev3
  - 2024 Q4: v2025pre
  - 2025 Q1: v2025.0
  - 2025 Q2: v2025.1
  - 2025 Q3: v2025.2
  - 2025 Q4: v2025.3
  - 2026 Q1: v2025.4
  - 2026 Q2: v2025.5
  - 2026 Q3: v2025.6
  - 2026 Q4: v2025.7

### Release downloads

| [Apertis v2025dev0.0 images](https://images.apertis.org/release/v2025dev0/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2025dev0/v2025dev0.0/amd64/fixedfunction/apertis_ostree_v2025dev0-fixedfunction-amd64-uefi_v2025dev0.0.img.gz) | [hmi](https://images.apertis.org/release/v2025dev0/v2025dev0.0/amd64/hmi/apertis_ostree_v2025dev0-hmi-amd64-uefi_v2025dev0.0.img.gz) | [base SDK](https://images.apertis.org/release/v2025dev0/v2025dev0.0/amd64/basesdk/apertis_v2025dev0-basesdk-amd64-sdk_v2025dev0.0.ova) | [SDK](https://images.apertis.org/release/v2025dev0/v2025dev0.0/amd64/sdk/apertis_v2025dev0-sdk-amd64-sdk_v2025dev0.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2025dev0/v2025dev0.0/armhf/fixedfunction/apertis_ostree_v2025dev0-fixedfunction-armhf-uboot_v2025dev0.0.img.gz) | [hmi](https://images.apertis.org/release/v2025dev0/v2025dev0.0/armhf/hmi/apertis_ostree_v2025dev0-hmi-armhf-uboot_v2025dev0.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2025dev0/v2025dev0.0/arm64/fixedfunction/apertis_ostree_v2025dev0-fixedfunction-arm64-uboot_v2025dev0.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2025dev0/v2025dev0.0/arm64/fixedfunction/apertis_ostree_v2025dev0-fixedfunction-arm64-rpi64_v2025dev0.0.img.gz) | [hmi](https://images.apertis.org/release/v2025dev0/v2025dev0.0/arm64/hmi/apertis_ostree_v2025dev0-hmi-arm64-rpi64_v2025dev0.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "virtualbox.md" >}} ).

#### Apertis v2025dev0 package list

The full list of packages available from the v2025dev0 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2025dev0](https://infrastructure.pages.apertis.org/dashboard/tsv/v2025dev0.tsv)

#### Apertis v2025dev0 repositories

    deb https://repositories.apertis.org/apertis/ v2025dev0 target development sdk non-free

## New features

### Include builddeps reports

Following the idea of provide more resources for Software Build of Materials, now Apertis provides
a report for the build dependencies of every package in the target images, including its version.
This information helps to easily track CVEs that might potentially affect Apertis packages.

### Support for running integration tests on MRs

Following the guidelines described in the [Apertis test strategy]({{< ref "test-strategy.md" >}})
this release provides initial support for running automated tests on LAVA in the
context of a MR. With this feature, packages changes can be validated by running integration tests
before landing them to main branches. 

It is currently recommended that only packages where potential regressions
may have a high-impact on the system stability have this feature enabled, given the extra
resources required to run the tests.

As before, all available integration tests are also run regularly on daily image builds.

## Build and integration

No new features

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

No known breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025dev0-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025dev0-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025dev0-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025dev0-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025dev0-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2025dev0-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #363](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/363) aum-ota-rollback-blacklist: test failed
- [Issue #364](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/364) aum-offline-upgrade-branch: test failed
- [Issue #365](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/365) aum-ota-auto: test failed
- [Issue #366](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/366) aum-ota-signed: test failed
- [Issue #371](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/371) aum-out-of-space: test failed
- [Issue #411](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/411) aum-ota-out-of-space: test failed
- [Issue #419](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/419) High level tracking of AUM issues
- [Issue #431](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/431) Package rust-coreutils does not provide license mapping information
- [Issue #437](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/437) Weekly images are not linked properly
- [Issue #448](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/448) ade-commands: test failed
- [Issue #451](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/451) dbus-installed-tests: test failed
- [Issue #456](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/456) SBOM support for rust packages does not work in v2024pre
- [Issue #461](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/461) sdk-debos-image-building: test failed
- [Issue #466](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/466) AUM out of space tests fail on amd64
- [Issue #469](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/469) aum-power-cut: test failed
- [Issue #474](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/474) "WiFi WPA3-Transition-Mode access point" (AP mode) connection is not re-established after rebooting AP.
- [Issue #476](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/476) "apertis-update-manager-usb-unplug" is getting failed. Update is not starting automatically
- [Issue #479](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/479) apparmor-basic-profiles: test failed
- [Issue #480](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/480) aum-ota-api: test failed
- [Issue #485](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/485) AUM tests fail on v2024pre armhf
- [Issue #487](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/487) "sdk-vb-fullscreen" fails on "SDK" and "BASE-SDK" of "v2023.3rc1".
- [Issue #488](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/488) Flashing an HMI image on the eMMC of R-car H3e-2G board leads to a kernel panic.
- [Issue #490](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/490) gpgv (sequioa) crashes when added debian archives
- [Issue #492](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/492) Applications are not present on the launcher screen in HMI image.

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #246](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/246) tiny-container-user-connectivity-profile: test failed
- [Issue #252](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/252) tiny-container-system-basic: test failed
- [Issue #330](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/330) datefudge: 64-bit time_t support on 32-bit archs
- [Issue #333](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/333) tiny-container-user-aa-enforcement: test failed
- [Issue #337](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/337) flatpak-run-demo-cli-app: test failed
- [Issue #338](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/338) psdk-test not executing properly.
- [Issue #339](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/339) Up Squared 6000 board fails to boot randomly
- [Issue #342](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/342) tiny-container-system-aa-enforcement: test failed
- [Issue #344](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/344) tiny-container-user-basic: test failed
- [Issue #377](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/377) import-debian-package issues during package import
- [Issue #378](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/378) apparmor-gstreamer1-0: test failed
- [Issue #396](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/396) Pipeline to build package fails during build-source for texlive-extra
- [Issue #405](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/405) traprain: test failed
- [Issue #413](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/413) obs-runner: Disabled architectures on a OBS package get re-enabled when CI creates a branch
- [Issue #415](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/415) sdk-vb-fullscreen test case is failing.
- [Issue #438](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/438) pstore-test case is getting failed in armhf
- [Issue #441](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/441) tiny-container-user-device-sharing: test failed
- [Issue #455](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/455) tiny-container-system-folder-sharing: test failed
- [Issue #459](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/459) tiny-container-system-device-sharing: test failed
- [Issue #465](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/465) tiny-container-system-seccomp: test failed
- [Issue #486](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/486) "webkit2gtk-ac-3d-rendering" fails in "ARMHF" of "v2023.3rc1".
- [Issue #491](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/491) AM62x: Do not install firmware prerequisites in image for boot firmware generation

### Low
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #382](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/382) Investigate why gitlab is sending warnings about new sign ons from internal IPs
