+++
date = "2020-10-01"
lastmod = "2024-12-19"
weight = 100

title = "Releases"
+++

This section contains the release notes and schedules for all current and previous releases of Apertis. Please refer to the [release schedule]({{< ref "releases.md" >}}) and [release flow]({{< ref "release-flow.md" >}}) for more information.

Next planned releases:

|Milestone                                  | v2024.4       | v2025.0       | v2026dev1     |
|---                                        | ---           | ---           | ---           |
|Start of Release Cycle                     | 2025-01-01    | 2025-01-01    | 2025-01-01    |
|Soft Feature Freeze                        | 2025-01-29    | 2025-02-05    | 2025-02-12    |
|Hard Feature Freeze / Soft Code Freeze     | 2025-02-05    | 2025-02-12    | 2025-02-19    |
|Release Candidate (RC1) / Hard Code Freeze | 2025-02-12    | 2025-02-19    | 2025-02-26    |
|Release                                    | 2025-02-27    | 2025-03-06    | 2025-03-13    |
