+++
date = "2023-02-27"
weight = 100

title = "v2023.4 Release Notes"

aliases = [
    "/release/v2023.4/releasenotes",
]
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2023.4** is the fifth **stable** release of the Apertis
v2023 stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2023 release stream up to the end
of 2024.

This Apertis release is built on top of Debian Bullseye with several
customizations and the Linux kernel 6.1.x LTS series.

Test results for the v2023.4 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2023/20240214.0215/apt)
  - [OSTree images](https://qa.apertis.org/report/v2023/20240214.0215/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2023/20240214.02155/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2023/20240214.0215/lxc)

## Release flow

  - 2021 Q4: v2023dev0
  - 2022 Q1: v2023dev1
  - 2022 Q2: v2023dev2
  - 2022 Q3: v2023dev3
  - 2022 Q4: v2023pre
  - 2023 Q1: v2023.0
  - 2023 Q2: v2023.1
  - 2023 Q3: v2023.2
  - 2023 Q4: v2023.3
  - **2024 Q1: v2023.4**
  - 2024 Q2: v2023.5
  - 2024 Q3: v2023.6
  - 2024 Q4: v2023.7

### Release downloads

| [Apertis v2023.4 images](https://images.apertis.org/release/v2023/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2023/v2023.4/amd64/fixedfunction/apertis_ostree_v2023-fixedfunction-amd64-uefi_v2023.4.img.gz) | [hmi](https://images.apertis.org/release/v2023/v2023.4/amd64/hmi/apertis_ostree_v2023-hmi-amd64-uefi_v2023.4.img.gz) | [base SDK](https://images.apertis.org/release/v2023/v2023.4/amd64/basesdk/apertis_v2023-basesdk-amd64-sdk_v2023.4.ova) | [SDK](https://images.apertis.org/release/v2023/v2023.4/amd64/sdk/apertis_v2023-sdk-amd64-sdk_v2023.4.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023/v2023.4/armhf/fixedfunction/apertis_ostree_v2023-fixedfunction-armhf-uboot_v2023.4.img.gz) | [hmi](https://images.apertis.org/release/v2023/v2023.4/armhf/hmi/apertis_ostree_v2023-hmi-armhf-uboot_v2023.4.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023/v2023.4/arm64/fixedfunction/apertis_ostree_v2023-fixedfunction-arm64-uboot_v2023.4.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2023/v2023.4/arm64/fixedfunction/apertis_ostree_v2023-fixedfunction-arm64-rpi64_v2023.4.img.gz) | [hmi](https://images.apertis.org/release/v2023/v2023.4/arm64/hmi/apertis_ostree_v2023-hmi-arm64-rpi64_v2023.4.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "virtualbox.md" >}} ).

#### Apertis v2023 package list

The full list of packages available from the v2023 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2023](https://infrastructure.pages.apertis.org/dashboard/tsv/v2023.tsv)

#### Apertis v2023 repositories

    deb https://repositories.apertis.org/apertis/ v2023 target development sdk

## Changes

This is a point release in the stable cycle, only security fixes and
small changes are appropriate for this release stream.

This release includes the security updates from Debian Bullseye and the latest
LTS Linux kernel on the 6.1.x series.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations.

### Breaks

No known breaks.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #339](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/339) Up Squared 6000 board fails to boot randomly in LAVA
- [Issue #363](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/363) aum-ota-rollback-blacklist: test failed
- [Issue #419](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/419) High level tracking of AUM issues
- [Issue #431](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/431) Package rust-coreutils does not provide license mapping information
- [Issue #437](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/437) Weekly images are not linked properly
- [Issue #451](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/451) dbus-installed-tests: test failed
- [Issue #466](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/466) AUM out of space tests fail on amd64
- [Issue #469](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/469) aum-power-cut: test failed
- [Issue #474](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/474) "WiFi WPA3-Transition-Mode access point" (AP mode) connection is not re-established after rebooting AP.
- [Issue #476](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/476) "apertis-update-manager-usb-unplug" is getting failed. Update is not starting automatically
- [Issue #480](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/480) aum-ota-api: test failed
- [Issue #487](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/487) "sdk-vb-fullscreen" fails on "SDK" and "BASE-SDK" of "v2023.3rc1".
- [Issue #514](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/514) aum-out-of-space: test failed
- [Issue #523](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/523) Dev package name showing inside the target on target source list
- [Issue #528](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/528) OBS runner creates conflicts in the origin repo, then waits forever for them
- [Issue #530](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/530) aum-ota-out-of-space: test failed

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #9](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/9) System users are shipped in /usr/etc/passwd instead of /lib/passwd
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #66](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/66) Some binaries in the toolchain tarball are huge
- [Issue #114](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/114) ci-package-builder tests fail due to OBS branches not being cleaned up
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #219](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/219) Eclipse-ide-cpp can't show preview of markdown files (.md)
- [Issue #230](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/230) WebProcess CRASHED error is seen on executing webkit related testcases
- [Issue #246](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/246) tiny-container-user-connectivity-profile: test failed
- [Issue #273](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/273) Debos build output hidden on GitLab job view can only be seen in the raw log file
- [Issue #278](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/278) chksum report fail! logs are seen when enabling WiFi dongle on the R-Car boards
- [Issue #331](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/331) Frequent 504 Errors repeatedly causing pipelines to be marked as failed
- [Issue #338](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/338) psdk-test not executing properly.
- [Issue #357](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/357) Instability issue in  bluez-phone test case for Up Squared 6000 board
- [Issue #377](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/377) import-debian-package issues during package import
- [Issue #378](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/378) apparmor-gstreamer1-0: test failed
- [Issue #396](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/396) Pipeline to build package fails during build-source for texlive-extra
- [Issue #397](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/397) Pipeline to import new version of plotly from Bookworm fails during pull-updates
- [Issue #413](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/413) obs-runner: Disabled architectures on a OBS package get re-enabled when CI creates a branch
- [Issue #438](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/438) pstore-test case is getting failed in armhf
- [Issue #441](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/441) tiny-container-user-device-sharing: test failed
- [Issue #459](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/459) tiny-container-system-device-sharing: test failed
- [Issue #462](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/462) agl-compositor mute functionality not working
- [Issue #463](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/463) Different behavior observed on running different video applications in different order.
- [Issue #486](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/486) "webkit2gtk-ac-3d-rendering" fails in "ARMHF".
- [Issue #488](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/488) Flashing an HMI image on the eMMC of R-car H3e-2G board leads to a kernel panic.
- [Issue #491](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/491) AM62x: Do not install firmware prerequisites in image for boot firmware generation
- [Issue #508](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/508) sdk-import-debian-package: test failed
- [Issue #525](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/525) RPi image list points to v2022 instead of daily development images

### Low
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #65](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/65) Terminal at xfce desktop corrupted after debos call
- [Issue #206](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/206) AUM rollback tests fail on UP Squared 6000 board
- [Issue #211](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/211) AUM power cut tests fail on UP Squared 6000 board
- [Issue #270](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/270) Issue in playing the Video on HMI armhf

