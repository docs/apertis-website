+++
date = "2023-09-07"
weight = 100

title = "v2023.3 Release schedule"

aliases = [
    "/release/v2023.3/release_schedule",
]
+++

The v2023.3 release cycle is scheduled to start in October 2023.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2023-10-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2023-11-01        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2023-11-08        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2023-11-15        |
| RC testing                                                                                               | 2023-11-16..11-29 |
| v2023.3 release                                                                                          | 2023-11-30        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
