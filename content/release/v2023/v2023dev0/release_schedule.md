+++
date = "2021-09-16"
weight = 100

title = "v2023dev0 Release schedule"

aliases = [
    "/release/v2023dev0/release_schedule",
]
+++

The v2023dev0 release cycle started in October 2021.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2021-10-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2021-11-24        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2021-12-01        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2021-12-08        |
| RC testing                                                                                               | 2021-12-09..12-15 |
| v2023dev0 release                                                                                        | 2021-12-16        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
