+++
date = "2017-12-22"
weight = 100

title = "17.12 ReleaseNotes"

aliases = [
    "/old-wiki/17.12/ReleaseNotes",
    "/release/17.12/releasenotes"
]
status = "Deprecated"
statusDescription = "This release of Apertis has been deprecated. Links to images and other resources have either been removed or are likely to be dead."
+++

# Apertis 17.12 Release

**17.12** is the current stable development release of **Apertis**, a
Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (both in the 32bit ARMv7 version using
the hardfloat ABI and in the 64-bit ARMv8 version) and Intel x86-64
(64-bit) systems.

This Apertis release is based on top of the Ubuntu 16.04 (Xenial) LTS
release with several customization. Test results of the 17.09 release
are available in the [17.12 test
report]().

### Release downloads

| [Apertis 17.12 images]() |
| ----------------------------------------------------------------- |
| Intel 64-bit                                                      |
| ARM 32-bit                                                        |
| ARM 64-bit                                                        |

The Intel `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "virtualbox.md" >}} ).

#### Apertis 17.12 repositories

` $ deb `<https://repositories.apertis.org/apertis/>` 17.12 target helper-libs development sdk hmi`

## New features

### ARM 64 bit support

Support for the ARM 64 bit (AArch64) architecture has been added to the
regularly built set of images, focusing on the `minimal` variant.

### LXC template

A new [template script for
LXC](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/-/blob/17.12/lxc/readme.md)
is now available to make instantiating LXC containers based on Apertis
as simple as possible: <code>

    sudo lxc-create -t $PWD/apertis-image-recipes/lxc/lxc-apertis-ostree --name apertis-test \
      -- --ospack https://images.apertis.org/lxc/17.12/20171221.0/lxc-ostree-17.12-amd64-minimal_20171221.0.tar.gz
    sudo lxc-start -F --name apertis-test

</code>

### New image building pipeline based on `debos`

A completely new infrastructure to build the Apertis images, ospacks,
and sysroots has now been put into production. The previous setup based
on `cb_build` has been replaced by new pipelines based on
[`debos`](https://github.com/go-debos/debos), which provides more
flexibility in customizing each image variant for the specific task at
hand and does so [with YAML-based
recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/-/blob/17.12/README)
which are significantly easier to understand and modify than the
shell-based configuration setup used by the [previous
toolchain](https://git.apertis.org/cgit/apertis-image-customization.git/tree/).

### Merged `/usr` support

Apertis now supports running in a setup where the [filesystem hierarchy
has been merged under
`/usr`](https://www.freedesktop.org/wiki/Software/systemd/TheCaseForTheUsrMerge/),
making easier for platform developers to isolate the read-only parts of
the operating system from the writable runtime data.

### Preliminary OSTree support

Support for OSTree-based deployments is being progressively introduced.
OSTree repositories are currently [populated as a step in the new
image-building
pipeline](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/-/blob/17.12/apertis-ostree-commit.yaml).
The previously mentioned LXC containers templates already make use of
them, providing robust, autonomous upgrades to the instantiated
containers.

### Migration to LAVAv2

With the end of the LAVAv1 development, the Apertis infrastructure has
been switched to LAVAv2. This has brough

## Groundwork

### Testcases clean up

The set of testcases being regularly run has been pruned of some old, no
longer relevant entries, to free resources to run tests on all supported
image variants.

### Merge-our-misc

The Merge-our-misc instance feeding updates
from Ubuntu LTS has been reactivated and configured on a dedicated host.

## Design and documentation

### IDC Connectivity domain

The [Inter-Domain Communication design
document]( {{< ref "inter-domain-communication.md" >}} )
has been extended to describe setups including a Connectivity domain,
used by Automotive domains to increase the isolation of network-facing
services.

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

#### Mildenhall UI framework

<dl>

<dt>

The Mildenhall UI toolkit is deprecated in favour of other UI toolkits
such as GTK+, Qt, or HTML5-based solutions

<dd>

From the next release Apertis may no longer ship the Mildenhall UI
toolkit nor any accompanying libraries, as the underlying Clutter
library has been deprecated upstream for some years now.

</dl>

#### Application framework

<dl>

<dt>

`ribchester-full` and `canterbury-full` are deprecated

<dd>

From the next release Canterbury and Ribchester will only offer the APIs
provided by the respective `-core` and the additional APIs offered by
the `-full` packages will be dropped

</dl>

### Breaks

<dl>

<dt>

LAVAv1 test job definitions are no longer supported

<dd>

Test job definitions using the JSON-based LAVAv1 syntax are no longer
supported and have been replaced by the LAVAv2 syntax based on YAML and
Jinja2 templating.

<dt>

Rollback mechanism no longer available

<dd>

The incomplete upgrade and rollback mechanism for system updates based
on Btrfs subvolumes has been dropped for this release. A more robust and
flexible mechanism based on OSTree is planned for the coming releases.

</dl>

## Infrastructure

### Apertis infrastructure tools

For Debian Jessie based systems:

` $ deb `<https://repositories.apertis.org/debian/>` jessie tools`

For Ubuntu Trusty based systems:

` $ deb `<https://repositories.apertis.org/ubuntu/>` trusty tools`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

## Known issues

### High (3)

  - Cross-compiling fails using ADE on AMD64 SDK

  - Remove bash version 4.3 from arm images

  - check-dbus-services: Navigation and Traprain tests fail for SDK
    image

### Normal (134)

  - Harbormaster always fail with commit message error

  - check-dbus-services: Error:
    GDBus.Error:org.freedesktop.DBus.Error.AccessDenied

  - apparmor-pulseaudio: pulseaudio.normal.expected_underlying_tests
    fails

  - frome: \`/tmp/frome-\*': No such file or directory

  - do-branching fails at a late stage cloning OBS binary repos

  - ribchester: Job for generated-test-case-ribchester.service canceled

  - canterbury: core-as-root and full-as-root tests failed

  - traprain: sadt: error: cannot find debian/tests/control

  - No music and Video files are present in the image

  - tracker-indexing-mass-storage test case fails

  - boot-no-crashes: ANOM_ABEND found in journalctl logs

  - dbus-installed-tests: service failed because a timeout was exceeded

  - sqlite: Many tests fail for target amd64, armhf and sdk

  - didcot: test_open_uri: assertion failed

  - tracker-indexing-local-storage: AssertionError: False is not true

  - gupnp-services: browsing and introspection tests fail

  - apparmor-gstreamer1.0: gstreamer1.0-decode: assertion failed

  - apparmor-folks: Several tests fail

  - apparmor: /usr/lib/apparmor/tests/environ fails

  - apparmor-tracker: AssertionError: False is not true

  - folks-search-contacts: folks-search-contacts_sh.service failed

  - folks-metacontacts-antilinking:
    folks-metacontacts-antilinking_sh.service failed

  - folks-alias-persistence: folks-alias-persistence_sh.service failed

  - folks-telepathy: folks-retrieve-contacts-telepathy_sh.service
    failed

  - folks-metacontacts: folks-metacontacts-linking_sh.service failed

  - Need vim.tiny and scp packages to be pre-installed as part of the
    target minimal images

  - Boot chart doesn't get generated

  - Blank screen seen on executing last-resort-message testcase

  - Building cargo and rustc in 17.12 for Firefox Quantum 57.0

  - No splashscreen on boot

  - Volume Control application hangs on opening it

  - Debos: changes 'resolv.conf' during commands execution chroot

  - ribchester-core causes apparmor denies on non-btrfs minimal image

  - Debos: option for kernel command parameters for 'filesystem-deplay'
    action

  - Debos: mountpoints which are mounted only in build time

  - Debos: allow to use partition number for 'raw' action

  - Preseed action is needed for Debos

  - zoom feature is not working as expected

  - Segmentation fault occurs while exectuing webkit2gtk-aligned-scroll
    test case

  - gnutls depends on old abandoned package gmp4 due to licensing
    reasons

  - introspectable support for GObject property, signal and methods

  - Crash observed on seed module when we accesing the D-Bus call method

  - Containers fail to load on Gen4 host

  - Crash observed on webruntime framework

  - Incorrect network available state shown in GNetworkMonitor

  - Observing multiple service instances in all 17.06 SDK images

  - Frampton application doesn't load when we re-launch them after
    clicking the back button

  - Rendering issue observed on websites like
    <http://www.moneycontrol.com>

  - Render theme buttons are not updating with respect to different zoom
    levels

  - HTML5 demo video's appear flipped when played on webkit2 based
    browser app

  - Page rendering is not smooth in sites like www.yahoo.com

  - Mildenhall should install themes in the standard xdg data dirs

  - webkit2GTK crash observed flicking on webview from other widget

  - Newport test fails on minimal images

  - Avoid unconstrained dbus AppArmor rules in frome

  - Steps like pattern is seen in the background in songs application

  - virtual keyboard is not showing for password input field of any
    webpage

  - Shutdown not working properly in virtualbox

  - bredon-0-launcher should be shipped in its own package, not in
    libbredon-0-1"

  - webview-test should be shipped in libbredon-0-tests instead of
    libbredon-0-1

  - libbredon/seed uninstallable on target as they depend on libraries
    in :development

  - Mismatching gvfs/gvfs-common and libatk1.0-0/libatk1.0-data package
    versions in the archive

  - MildenhallSelPopupItem model should be changed to accept only gchar
    \* instead of MildenhallSelPopupItemIconDetail for icons

  - Rhosydd service crashes when client exits on some special usecases
    (Refer description for it)

  - rhosydd-client crashes when displaying vehicle properties for mock
    backend

  - libmildenhall-0-0 contains files that would conflict with a future
    libmildenhall-0-1

  - libshoreham packaging bugs

  - arm-linux-gnueabihf-pkg-config does not work with sysroots installed
    by \`ade\`

  - MildenhallSelectionPopupItem doesn't take ownership when set
    properties

  - In mildenhall, URL history speller implementation is incomplete.

  - Variable roller is not working

  - Compositor hides the other screens

  - Status bar is not getting updated with the current song/video being
    played

  - canterbury: Most of the tests fail

  - ribchester: gnome-desktop-testing test times out

  - rhosydd: integration test fails

  - didcot-client: autopkgtest fails with
    org.apertis.Didcot.Error.NoApplicationFound and ""Unit
    didcot.service could not be found""

  - GtkClutterLauncher: Segfaults with mouse right-clicking

  - Cross debugging through GDB and GDBserver Not possible.

  - Voice/Audio is not heard for the Bluez-hfp profile in i.MX6

  - Deploying LAVA tests for SDK images on personal stream fails

  - The web runtime doesn't set the related view when opening new
    windows

  - Segmentation fault when disposing test executable of mildenhall

  - <abstractions/chaiwala-base> gives privileges that not every
    app-bundle should have

  - Canterbury entry-point launching hides global popups, but only
    sometimes

  - Album art is missing in one of the rows of the songs application

  - folks: random tests fail

  - GLib, GIO Reference Manual links are incorrectly swapped

  - GObject Generator link throws 404 error

  - Search feature doesn't work correctly for appdev portal located at
    `https://appdev.apertis.org/documentation/index.html`

  - cgroups-resource-control: test network-cgroup-prio-class failed

  - webview Y offset not considered to place, full screen video on
    youtube webpage

  - Confirm dialog status updated before selecting the confirm option
    YES/NO

  - gupnp-services tests test_service_browsing and
    test_service_introspection fail on target-arm image

  - telepathy-gabble-tests should depend on python-dbus

  - libgles2-vivante-dev is not installable

  - mx6qsabrelite: linking issue with libgstimxeglvivsink.so and
    libgstimxvpu.so gstreamer plugins

  - apparmor-folks: unable to link contacts to test unlinking

  - tracker tests: Error creating thumbnails: No poster key found in
    metadata

  - Cannot open links within website like yahoo.com

  - polkit-parsing: TEST_RESULT:fail

  - make check fails on libbredon package for wayland warnings

  - Ribchester: deadlock when calling RemoveApp() right after RollBack()

  - Canterbury messes up kerning when .desktop uses unicode chars

  - VirtualBox freezes using 100% CPU when resuming the host from
    suspend

  - VirtualBox display freezes when creating multiple notifications at
    once and interacting (hover and click) with them

  - apparmor-tracker: underlying_tests failed

  - tracker-indexing-local-storage: Stopping tracker-store services

  - Clang package fails to install appropriate egg-info needed by hotdoc

  - Unusable header in Traprain section in Devhelp

  - Test apps are failing in Liblightwood with the use of GTest

  - Focus in launcher rollers broken because of copy/paste errors

  - Mismatch between server version file and sysroot version

  - Pulse Audio volume control doesn't launch as a separate window on
    SDK

  - Crash when initialising egl on ARM target

  - gupnp-services: test service failed

  - "shapwick reads /etc/nsswitch.conf and /etc/passwd

  - factory reset with a different image's rootfs.tar.gz results in
    emergency mode

  - GStreamer playbin prioritises imxeglvivsink over
    clutterautovideosink

  - Fix folks EDS tests to not be racy

  - Interaction with PulseAudio not allowed by its AppArmor profile

  - libsoup-unit: ssl-test failed for ARM

  - telepathy-gabble: Several tests failed

  - minimal image: DISPLAY sanity check tests failed

  - Unsupported launguage text is not shown on the page in
    GtkClutterLauncher

  - Target doesn't reboot after system update

  - cgroups-resource-control: blkio-weights tests failed

  - mildenhall-settings: does not generate localization files from
    source

  - libgrassmoor: executes tracker-control binary

  - factory-reset-tool TC: flagcheck messages are hidden by Plymouth

  - apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail \[Bugzilla bug \#331\]

  - remove INSTALL, aclocal.m4 files from langtoft

  - No connectivity Popup is not seen when the internet is disconnected.

  - Not able to load heavy sites on GtkClutterLauncher

### Low (45)

  - Inital Roller mappings are misaligned on the HMI

  - Segmentation fault is observed on closing the mildenhall-compositor

  - On first flashing of the image any second application does not get
    displayed (eg. album art)

  - On multiple re-entries from settings to eye the compositor hangs

  - connman: patch ""device: Don't report EALREADY"" not accepted
    upstream

  - connman: patch ""Use ProtectSystem=true"" rejected upstream

  - Roller problem in settings application

  - Songs do not start playing from the beginning but instead start a
    little ahead

  - Blank screen seen in Songs application on re-entry

  - qemu-arm-static not found in the pre installed qemu-user-static
    package

  - Resizing the window causes page corruption

  - Content on a webpage doesn't load in sync with the scroll bar

  - Spacing issues between text and selection box in website like amazon

  - Compositor seems to hide the bottom menu of a webpage

  - gstreamer1-0-decode: Failed to load plugin warning

  - Album Art Thumbnail missing in Thumb view in ARTIST Application

  - "If 2 drawers are activated, the most recent one hides behind the
    older one, instead of coming on top of older one.

  - beep audio decoder gives errors continously

  - Images for the video links are not shown in news.google.com on
    GtkClutterLauncher

  - Context drawer displced when switching to fullscreen in browser app

  - Printing hotdoc webpage directly results in misformatted document

  - Zoom in feature does not work on google maps

  - Cannot open/view pdf documents in browser (GtkClutterLauncher)

  - Drop down lists are not working on a site like facebook

  - Background video is not played in some website with
    GtkClutterLauncher

  - "Share links to facebook, twitter are nbt working in browser
    (GtkClutterLauncher)

  - The background HMI is blank on clicking the button for Power OFF

  - Horizontal scroll is not shown on GtkClutterLauncher

  - The video player window is split into 2 frames in default view

  - Back option is missing as part of the tool tip

  - Bluetooth pairing option not working as expected

  - Simulator screen is not in center but left aligned

  - Video doesn't play when toggling from full screen to detail view

  - Theme ,any F node which is a child of an E node is not working for
    Apertis widgets.

  - Videos are hidden when Eye is launched

  - bluetooth device pair failing with ""Invalid arguments in method
    call"""

  - Clutter_text_set_text API redraws entire clutterstage

  - mxkineticscrollview-smooth-pan:Free scroll doesn't work

  - Network search pop-up isn't coming up in wi-fi settings

  - Power button appers to be disabled on target

  - Documentation is not available from the main folder

  - Mildenhall compositor crops windows

  - Only half of the hmi is covered when opening gnome.org

  - Upstream: linux-tools-generic should depend on lsb-release

  - Remove unnecessary folks package dependencies for automated tests

### Lowest (2)

  - Broken HTML generation in Backworth for the developers portal
    website

  - Failed to load Captcha in Apertis developer portal
