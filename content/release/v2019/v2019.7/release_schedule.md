+++
date = "2021-03-18"
weight = 100

title = "v2019.7 Release schedule"

aliases = [
    "/release/v2019.7/release_schedule",
]
+++

The v2019.7 release cycle started in December 2020.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2021-03-18        |
| Soft feature freeze: end of feature proposal and review period                                           | 2021-05-12        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2021-05-19        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2021-05-26        |
| RC testing                                                                                               | 2021-05-27..06-02 |
| v2019.7 release                                                                                          | 2021-06-03        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
