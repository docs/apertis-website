+++
date = "2023-12-05"
weight = 100

title = "v2024pre Release Notes"

aliases = [
    "/release/v2024pre/releasenotes",
]
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2024pre** is the preview release of the Apertis
v2024 stable release flow that will lead to the LTS **Apertis v2024.0**
release in March 2024.

This is the second Apertis release that is built on top of Debian Bookworm
along with several customizations and it ships the latest Linux LTS kernel
6.6.x series. Later releases in the v2024 channel will be tracking kernel updates
in this LTS series as well as Debian Bookworm Stable Channel.

Test results for the v2024pre release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2024pre/20231122.0315/apt)
  - [OSTree images](https://qa.apertis.org/report/v2024pre/20231122.0315/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2024pre/20231122.0315/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2024pre/20231122.0315/lxc)

## Release flow

  - 2022 Q4: v2024dev0
  - 2023 Q1: v2024dev1
  - 2023 Q2: v2024dev2
  - 2023 Q3: v2024dev3
  - **2023 Q4: v2024pre**
  - 2024 Q1: v2024.0
  - 2024 Q2: v2024.1
  - 2024 Q3: v2024.2
  - 2024 Q4: v2024.3
  - 2025 Q1: v2024.4
  - 2025 Q2: v2024.5
  - 2025 Q3: v2024.6
  - 2025 Q4: v2024.7

### Release downloads

| [Apertis v2024pre.0 images](https://images.apertis.org/release/v2024pre/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2024pre/v2024pre.0/amd64/fixedfunction/apertis_ostree_v2024pre-fixedfunction-amd64-uefi_v2024pre.0.img.gz) | [hmi](https://images.apertis.org/release/v2024pre/v2024pre.0/amd64/hmi/apertis_ostree_v2024pre-hmi-amd64-uefi_v2024pre.0.img.gz) | [base SDK](https://images.apertis.org/release/v2024pre/v2024pre.0/amd64/basesdk/apertis_v2024pre-basesdk-amd64-sdk_v2024pre.0.ova) | [SDK](https://images.apertis.org/release/v2024pre/v2024pre.0/amd64/sdk/apertis_v2024pre-sdk-amd64-sdk_v2024pre.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024pre/v2024pre.0/armhf/fixedfunction/apertis_ostree_v2024pre-fixedfunction-armhf-uboot_v2024pre.0.img.gz) | [hmi](https://images.apertis.org/release/v2024pre/v2024pre.0/armhf/hmi/apertis_ostree_v2024pre-hmi-armhf-uboot_v2024pre.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024pre/v2024pre.0/arm64/fixedfunction/apertis_ostree_v2024pre-fixedfunction-arm64-uboot_v2024pre.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2024pre/v2024pre.0/arm64/fixedfunction/apertis_ostree_v2024pre-fixedfunction-arm64-rpi64_v2024pre.0.img.gz) | [hmi](https://images.apertis.org/release/v2024pre/v2024pre.0/arm64/hmi/apertis_ostree_v2024pre-hmi-arm64-rpi64_v2024pre.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "virtualbox.md" >}} ).

#### Apertis v2024pre package list

The full list of packages available from the v2024pre APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2024pre](https://infrastructure.pages.apertis.org/dashboard/tsv/v2024pre.tsv)

#### Apertis v2024pre repositories

    deb https://repositories.apertis.org/apertis/ v2024pre target development sdk non-free

## New features

### New versions of core packages

As part of the rebase on top of Debian Bookworm all the packages have been updated. As a summary this is a list
of the packages available:

- [GCC 12.2.0-14](https://gitlab.apertis.org/pkg/gcc-12/-/tree/apertis/v2024pre)
- [Glib 2.74.6-2](https://gitlab.apertis.org/pkg/glib2.0/-/tree/apertis/v2024pre)
- [Glibc 2.36-9+deb12u3](https://gitlab.apertis.org/pkg/glibc/-/tree/apertis/v2024pre)
- [Systemd 252.17-1~deb12u1](https://gitlab.apertis.org/pkg/systemd/-/tree/apertis/v2024pre)
- [OpenSSL 3.0.11-1~deb12u2](https://gitlab.apertis.org/pkg/openssl/-/tree/apertis/v2024pre)
- [GNU TLS 3.7.9-2](https://gitlab.apertis.org/pkg/gnutls28/-/tree/apertis/v2024pre)
- [Nginx 1.22.1-9](https://gitlab.apertis.org/pkg/nginx/-/tree/apertis/v2024pre)
- [Gstreamer 1.22.0-2](https://gitlab.apertis.org/pkg/gstreamer1.0/-/tree/apertis/v2024pre)
- [Pipewire 0.3.84-1~bpo12+1](https://gitlab.apertis.org/pkg/pipewire/-/tree/apertis/v2024pre)

The [Debian release notes](https://www.debian.org/releases/stable/releasenotes) provides additional information
about the new version of packages.

### Add support for podman

As working with different container technologies provides extra flexibility to build products, Apertis added
the support for podman (libpod). During this work, special care has been taken to ensure that this package and
its dependencies match [Apertis license expectations]({{< ref license-expectations >}}).

### Include user friendly license reports for target images

As part of the evolution of Apertis, a new set of reports based on [OSS Review Toolkit](https://github.com/oss-review-toolkit/ort)
are included for every target image. These reports are human friendly, allowing an easy review
of the licenses used by Apertis images.

### Include builddeps reports

Following the idea of provide more resources for Software Build of Materials, now Apertis provides
a report for the build dependencies of every package in the target images, including its version.
This information helps to easily track CVEs that might potentially affect Apertis packages.

### Support for running integration tests on MRs

Following the guidelines described in the [Apertis test strategy]({{< ref "test-strategy.md" >}})
this release provides initial support for running automated tests on LAVA in the
context of a MR. With this feature, packages changes can be validated by running integration tests
before landing them to main branches. 

It is currently recommended that only packages where potential regressions
may have a high-impact on the system stability have this feature enabled, given the extra
resources required to run the tests.

As before, all available integration tests are also run regularly on daily image builds.

## Build and integration

### Improve aptly integration and performance

After switching to aptly as the publisher for Apertis packages several improvements have been made
to the integration with OBS as well as the performance during publishing. As a result, the overhead
in the process of creating managing APT repositories has been dramatically reduced, both in the manual
effort required and in the amount of infrastructure resources.

### Support for more fine-grained access roles in QA Report App

With the goal of providing better access control to the QA Report App, now three different roles
are defined to allow performing the following actions: view results, submit manual tests results and
tag images. These changes should allow teams to better split responsibilities, by only allowing
the necessary access to specific users/groups. 

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

No known breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024pre-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024pre-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024pre-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024pre-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024pre-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024pre-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #363](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/363) aum-ota-rollback-blacklist: test failed
- [Issue #364](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/364) aum-offline-upgrade-branch: test failed
- [Issue #365](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/365) aum-ota-auto: test failed
- [Issue #366](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/366) aum-ota-signed: test failed
- [Issue #371](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/371) aum-out-of-space: test failed
- [Issue #411](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/411) aum-ota-out-of-space: test failed
- [Issue #419](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/419) High level tracking of AUM issues
- [Issue #431](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/431) Package rust-coreutils does not provide license mapping information
- [Issue #437](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/437) Weekly images are not linked properly
- [Issue #448](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/448) ade-commands: test failed
- [Issue #451](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/451) dbus-installed-tests: test failed
- [Issue #456](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/456) SBOM support for rust packages does not work in v2024pre
- [Issue #461](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/461) sdk-debos-image-building: test failed
- [Issue #466](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/466) AUM out of space tests fail on amd64
- [Issue #469](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/469) aum-power-cut: test failed
- [Issue #474](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/474) "WiFi WPA3-Transition-Mode access point" (AP mode) connection is not re-established after rebooting AP.
- [Issue #476](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/476) "apertis-update-manager-usb-unplug" is getting failed. Update is not starting automatically
- [Issue #479](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/479) apparmor-basic-profiles: test failed
- [Issue #480](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/480) aum-ota-api: test failed
- [Issue #485](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/485) AUM tests fail on v2024pre armhf
- [Issue #487](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/487) "sdk-vb-fullscreen" fails on "SDK" and "BASE-SDK" of "v2023.3rc1".
- [Issue #488](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/488) Flashing an HMI image on the eMMC of R-car H3e-2G board leads to a kernel panic.
- [Issue #490](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/490) gpgv (sequioa) crashes when added debian archives

Needs triage

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #246](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/246) tiny-container-user-connectivity-profile: test failed
- [Issue #252](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/252) tiny-container-system-basic: test failed
- [Issue #330](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/330) datefudge: 64-bit time_t support on 32-bit archs
- [Issue #333](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/333) tiny-container-user-aa-enforcement: test failed
- [Issue #337](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/337) flatpak-run-demo-cli-app: test failed
- [Issue #338](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/338) psdk-test not executing properly.
- [Issue #339](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/339) Up Squared 6000 board fails to boot randomly
- [Issue #342](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/342) tiny-container-system-aa-enforcement: test failed
- [Issue #344](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/344) tiny-container-user-basic: test failed
- [Issue #377](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/377) import-debian-package issues during package import
- [Issue #378](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/378) apparmor-gstreamer1-0: test failed
- [Issue #396](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/396) Pipeline to build package fails during build-source for texlive-extra
- [Issue #405](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/405) traprain: test failed
- [Issue #413](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/413) obs-runner: Disabled architectures on a OBS package get re-enabled when CI creates a branch
- [Issue #415](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/415) sdk-vb-fullscreen test case is failing.
- [Issue #438](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/438) pstore-test case is getting failed in armhf
- [Issue #441](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/441) tiny-container-user-device-sharing: test failed
- [Issue #455](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/455) tiny-container-system-folder-sharing: test failed
- [Issue #459](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/459) tiny-container-system-device-sharing: test failed
- [Issue #465](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/465) tiny-container-system-seccomp: test failed
- [Issue #486](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/486) "webkit2gtk-ac-3d-rendering" fails in "ARMHF" of "v2023.3rc1".
- [Issue #491](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/491) AM62x: Do not install firmware prerequisites in image for boot firmware generation

### Low
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #382](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/382) Investigate why gitlab is sending warnings about new sign ons from internal IPs
