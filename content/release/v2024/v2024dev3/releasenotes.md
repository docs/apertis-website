+++
date = "2023-09-13"
weight = 100

title = "v2024dev3 Release Notes"

aliases = [
    "/release/v2024dev3/releasenotes",
]
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2024dev3** is the fourth development release of the Apertis
v2024 stable release flow that will lead to the LTS **Apertis v2024.0**
release in March 2024.

This is the second Apertis release that is built on top of Debian Bookworm along with several
customizations. It currently ships with the Linux kernel 6.3.x series.
Later releases in the v2024 channel will be tracking newer kernel versions
up to the next LTS, as well as Debian Bookworm Stable Channel.


Test results for the v2024dev3 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2024dev3/20230830.0015/apt)
  - [OSTree images](https://qa.apertis.org/report/v2024dev3/20230830.0015/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2024dev3/20230830.0015/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2024dev3/20230830.0015/lxc)

## Release flow

  - 2022 Q4: v2024dev0
  - 2023 Q1: v2024dev1
  - 2023 Q2: v2024dev2
  - **2023 Q3: v2024dev3**
  - 2023 Q4: v2024pre
  - 2024 Q1: v2024.0
  - 2024 Q2: v2024.1
  - 2024 Q3: v2024.2
  - 2024 Q4: v2024.3
  - 2025 Q1: v2024.4
  - 2025 Q2: v2024.5
  - 2025 Q3: v2024.6
  - 2025 Q4: v2024.7

### Release downloads

| [Apertis v2024dev3.0 images](https://images.apertis.org/release/v2024dev3/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2024dev3/v2024dev3.0/amd64/fixedfunction/apertis_ostree_v2024dev3-fixedfunction-amd64-uefi_v2024dev3.0.img.gz) | [hmi](https://images.apertis.org/release/v2024dev3/v2024dev3.0/amd64/hmi/apertis_ostree_v2024dev3-hmi-amd64-uefi_v2024dev3.0.img.gz) | [base SDK](https://images.apertis.org/release/v2024dev3/v2024dev3.0/amd64/basesdk/apertis_v2024dev3-basesdk-amd64-sdk_v2024dev3.0.ova) | [SDK](https://images.apertis.org/release/v2024dev3/v2024dev3.0/amd64/sdk/apertis_v2024dev3-sdk-amd64-sdk_v2024dev3.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024dev3/v2024dev3.0/armhf/fixedfunction/apertis_ostree_v2024dev3-fixedfunction-armhf-uboot_v2024dev3.0.img.gz) | [hmi](https://images.apertis.org/release/v2024dev3/v2024dev3.0/armhf/hmi/apertis_ostree_v2024dev3-hmi-armhf-uboot_v2024dev3.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024dev3/v2024dev3.0/arm64/fixedfunction/apertis_ostree_v2024dev3-fixedfunction-arm64-uboot_v2024dev3.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2024dev3/v2024dev3.0/arm64/fixedfunction/apertis_ostree_v2024dev3-fixedfunction-arm64-rpi64_v2024dev3.0.img.gz) | [hmi](https://images.apertis.org/release/v2024dev3/v2024dev3.0/arm64/hmi/apertis_ostree_v2024dev3-hmi-arm64-rpi64_v2024dev3.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "virtualbox.md" >}} ).

#### Apertis v2024dev3 package list

The full list of packages available from the v2024dev3 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2024dev3](https://infrastructure.pages.apertis.org/dashboard/tsv/v2024dev3.tsv)

#### Apertis v2024dev3 repositories

    deb https://repositories.apertis.org/apertis/ v2024dev3 target development sdk non-free

## New features

### New versions of core packages

As part of the rebase on top of Debian Bookworm all the packages have been updated. As a summary this is a list
of the packages available:

- [GCC 12.2.0-14](https://gitlab.apertis.org/pkg/gcc-12/-/tree/apertis/v2024dev3)
- [Glib 2.74.6-1](https://gitlab.apertis.org/pkg/glib2.0/-/tree/apertis/v2024dev3)
- [Glibc 2.36-9](https://gitlab.apertis.org/pkg/glibc/-/tree/apertis/v2024dev3)
- [Systemd 252.6-1](https://gitlab.apertis.org/pkg/systemd/-/tree/apertis/v2024dev3)
- [OpenSSL 3.0.9-1](https://gitlab.apertis.org/pkg/openssl/-/tree/apertis/v2024dev3)
- [GNU TLS 3.7.9-1](https://gitlab.apertis.org/pkg/gnutls28/-/tree/apertis/v2024dev3)
- [Nginx 1.22.1-7](https://gitlab.apertis.org/pkg/nginx/-/tree/apertis/v2024dev3/debian)
- [Gstreamer 1.22.0-3](https://gitlab.apertis.org/pkg/gstreamer1.0/-/tree/apertis/v2024dev3)
- [Pipewire 0.3.65-3](https://gitlab.apertis.org/pkg/pipewire/-/tree/apertis/v2024dev3)

## Build and integration

No new features

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

No known breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev3-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev3-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev3-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev3-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev3-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev3-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #363](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/363) aum-ota-rollback-blacklist: test failed
- [Issue #364](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/364) aum-offline-upgrade-branch: test failed
- [Issue #365](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/365) aum-ota-auto: test failed
- [Issue #366](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/366) aum-ota-signed: test failed
- [Issue #368](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/368) aum-ota-api: test failed
- [Issue #371](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/371) aum-out-of-space: test failed
- [Issue #389](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/389) OTA test cases are failing on arm64 architecture.
- [Issue #411](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/411) aum-ota-out-of-space: test failed
- [Issue #419](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/419) High level tracking of AUM issues
- [Issue #425](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/425) Fonts Application (Fonts app) is not found in SDK image
- [Issue #426](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/426) aum-rollback-blacklist: test failed
- [Issue #430](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/430) Up Squared 6000 hangs at boot randomly
- [Issue #431](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/431) Package rust-coreutils does not provide license mapping information
- [Issue #436](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/436) "flatpak-run-demo-hmi-app" fails on SDK of v2024dev3
- [Issue #437](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/437) Weekly images are not linked properly
- [Issue #446](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/446) FTBS: Package strace fails to build
- [Issue #447](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/447) FTBS: Package devscripts fails to build

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #246](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/246) tiny-container-user-connectivity-profile: test failed
- [Issue #252](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/252) tiny-container-system-basic: test failed
- [Issue #289](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/289) Difference in license data observation between scan copyright and fossology tool data
- [Issue #329](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/329) ade-commands: test failed
- [Issue #330](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/330) datefudge: 64-bit time_t support on 32-bit archs
- [Issue #333](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/333) tiny-container-user-aa-enforcement: test failed
- [Issue #337](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/337) flatpak-run-demo-cli-app: test failed
- [Issue #340](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/340) sdk-import-debian-package: test failed
- [Issue #342](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/342) tiny-container-system-aa-enforcement: test failed
- [Issue #344](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/344) tiny-container-user-basic: test failed
- [Issue #377](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/377) import-debian-package issues during package import
- [Issue #378](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/378) apparmor-gstreamer1-0: test failed
- [Issue #395](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/395) Pipeline to import new version of exim4 from Bookworm fails during prepare-build-env
- [Issue #396](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/396) Pipeline to build package fails during build-source for texlive-extra
- [Issue #397](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/397) Pipeline to import new version of plotly from Bookworm fails during pull-updates
- [Issue #405](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/405) traprain: test failed
- [Issue #406](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/406) apparmor-geoclue: test failed
- [Issue #413](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/413) obs-runner: Disabled architectures on a OBS package get re-enabled when CI creates a branch
- [Issue #415](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/415) sdk-vb-fullscreen test case is failing on 2024dev3 image.

### Low
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #382](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/382) Investigate why gitlab is sending warnings about new sign ons from internal IPs
