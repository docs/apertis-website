+++
date = "2023-06-21"
weight = 100

title = "v2024dev2 Release Notes"

aliases = [
    "/release/v2024dev2/releasenotes",
]
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2024dev2** is the third development release of the Apertis
v2024 stable release flow that will lead to the LTS **Apertis v2024.0**
release in March 2024.

This is the first Apertis release that is built on top of Debian Bookworm along with several
customizations. It currently ships with the Linux kernel 6.1.x series.
Later releases in the v2024 channel will be tracking newer kernel versions
up to the next LTS, as well as Debian Bookworm Stable Channel.


Test results for the v2024dev2 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2024dev2/20230621.1604/apt)
  - [OSTree images](https://qa.apertis.org/report/v2024dev2/20230621.1604/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2024dev2/20230621.1604/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2024dev2/20230621.1604/lxc)

## Release flow

  - 2022 Q4: v2024dev0
  - 2023 Q1: v2024dev1
  - **2023 Q2: v2024dev2**
  - 2023 Q3: v2024dev3
  - 2023 Q4: v2024pre
  - 2024 Q1: v2024.0
  - 2024 Q2: v2024.1
  - 2024 Q3: v2024.2
  - 2024 Q4: v2024.3
  - 2025 Q1: v2024.4
  - 2025 Q2: v2024.5
  - 2025 Q3: v2024.6
  - 2025 Q4: v2024.7

### Release downloads

| [Apertis v2024dev2.0 images](https://images.apertis.org/release/v2024dev2/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2024dev2/v2024dev2.0/amd64/fixedfunction/apertis_ostree_v2024dev2-fixedfunction-amd64-uefi_v2024dev2.0.img.gz) | [hmi](https://images.apertis.org/release/v2024dev2/v2024dev2.0/amd64/hmi/apertis_ostree_v2024dev2-hmi-amd64-uefi_v2024dev2.0.img.gz) | [base SDK](https://images.apertis.org/release/v2024dev2/v2024dev2.0/amd64/basesdk/apertis_v2024dev2-basesdk-amd64-sdk_v2024dev2.0.ova) | [SDK](https://images.apertis.org/release/v2024dev2/v2024dev2.0/amd64/sdk/apertis_v2024dev2-sdk-amd64-sdk_v2024dev2.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024dev2/v2024dev2.0/armhf/fixedfunction/apertis_ostree_v2024dev2-fixedfunction-armhf-uboot_v2024dev2.0.img.gz) | [hmi](https://images.apertis.org/release/v2024dev2/v2024dev2.0/armhf/hmi/apertis_ostree_v2024dev2-hmi-armhf-uboot_v2024dev2.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024dev2/v2024dev2.0/arm64/fixedfunction/apertis_ostree_v2024dev2-fixedfunction-arm64-uboot_v2024dev2.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2024dev2/v2024dev2.0/arm64/fixedfunction/apertis_ostree_v2024dev2-fixedfunction-arm64-rpi64_v2024dev2.0.img.gz) | [hmi](https://images.apertis.org/release/v2024dev2/v2024dev2.0/arm64/hmi/apertis_ostree_v2024dev2-hmi-arm64-rpi64_v2024dev2.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "virtualbox.md" >}} ).

#### Apertis v2024dev2 package list

The full list of packages available from the v2024dev2 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2024dev2](https://infrastructure.pages.apertis.org/dashboard/tsv/v2024dev2.tsv)

#### Apertis v2024dev2 repositories

    deb https://repositories.apertis.org/apertis/ v2024dev2 target development sdk non-free

## New features

### New versions of core packages

As part of the rebase on top of Debian Bookworm all the packages have been updated. As a summary this is a list
of the packages available:

- [GCC 12.2.0-14](https://gitlab.apertis.org/pkg/gcc-12/-/tree/apertis/v2024dev2)
- [Glib 2.74.6-1](https://gitlab.apertis.org/pkg/glib2.0/-/tree/apertis/v2024dev2)
- [Glibc 2.36-9](https://gitlab.apertis.org/pkg/glibc/-/tree/apertis/v2024dev2)
- [Systemd 252.6-1](https://gitlab.apertis.org/pkg/systemd/-/tree/apertis/v2024dev2)
- [OpenSSL 3.0.9-1](https://gitlab.apertis.org/pkg/openssl/-/tree/apertis/v2024dev2)
- [GNU TLS 3.7.9-1](https://gitlab.apertis.org/pkg/gnutls28/-/tree/apertis/v2024dev2)
- [Nginx 1.22.1-7](https://gitlab.apertis.org/pkg/nginx/-/tree/apertis/v2024dev2/debian)
- [Gstreamer 1.22.0-3](https://gitlab.apertis.org/pkg/gstreamer1.0/-/tree/apertis/v2024dev2)
- [Pipewire 0.3.65-3](https://gitlab.apertis.org/pkg/pipewire/-/tree/apertis/v2024dev2)

## Build and integration

No new features

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

No known breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev2-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev2-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev2-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev2-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev2-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024dev2-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #190](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/190) aum-power-cut: test failed
- [Issue #314](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/314) QA Report App: Invalid links to images on Test Report pages
- [Issue #330](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/330) datefudge: 64-bit time_t support on 32-bit archs
- [Issue #336](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/336) "lcov" source package: entry "Multi-Arch: foreign" missing in debian/control file
- [Issue #339](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/339) Up Squared 6000 board fails to boot randomly
- [Issue #350](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/350) secure-boot-imx6 test case Pre-Conditions upgradation
- [Issue #360](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/360) dwarf2sources fails to parse DWARF info when using DWARF-5

### Needs triage
- [Issue #348](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/348) R-car R8A7795 is unable to boot latest images

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #246](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/246) tiny-container-user-connectivity-profile: test failed
- [Issue #252](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/252) tiny-container-system-basic: test failed
- [Issue #291](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/291) tiny-container-system-folder-sharing: test failed
- [Issue #318](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/318) Image pipeline tries to submit LAVA test on bootstrap tests
- [Issue #322](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/322) wrap-and-sort fails to parse some debian/control.in files
- [Issue #324](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/324) ci-license-scan does not fail on UNKNOWN
- [Issue #329](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/329) ade-commands: test failed
- [Issue #333](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/333) tiny-container-user-aa-enforcement: test failed
- [Issue #337](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/337) flatpak-run-demo-cli-app: test failed
- [Issue #340](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/340) sdk-import-debian-package: test failed
- [Issue #342](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/342) tiny-container-system-aa-enforcement: test failed
- [Issue #344](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/344) tiny-container-user-basic: test failed
- [Issue #353](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/353) gettext-i18n: test failed
- [Issue #354](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/354) apparmor utils test shows as incomplete
- [Issue #357](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/357) Instability issue in  bluez-phone test case for Up Squared 6000 board

### Low
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #38](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/38) LAVA/Phab bridge timeouts
- [Issue #45](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/45) Manual testcase results should not have any hyperlink in the LavaPhabridge report page

