+++
date = "2024-03-07"
weight = 100

title = "v2024.0 Release Notes"

aliases = [
    "/release/v2024.0/releasenotes",
]
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2024.0** is the first **stable** release of the Apertis
v2024 stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2024 release stream up to the end
of 2025.

This is the first Apertis release that is built on top of Debian Bookworm
along with several customizations and it ships the latest Linux LTS kernel
6.6.x series. Later releases in the v2024 channel will be tracking kernel updates
in this LTS series as well as Debian Bookworm Stable Channel.

Test results for the v2024.0 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2024/20240221.0115/apt)
  - [OSTree images](https://qa.apertis.org/report/v2024/20240221.0115/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2024/20240221.0115/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2024/20240221.0115/lxc)

## Release flow

  - 2022 Q4: v2024dev0
  - 2023 Q1: v2024dev1
  - 2023 Q2: v2024dev2
  - 2023 Q3: v2024dev3
  - 2023 Q4: v2024pre
  - **2024 Q1: v2024.0**
  - 2024 Q2: v2024.1
  - 2024 Q3: v2024.2
  - 2024 Q4: v2024.3
  - 2025 Q1: v2024.4
  - 2025 Q2: v2024.5
  - 2025 Q3: v2024.6
  - 2025 Q4: v2024.7

### Release downloads

| [Apertis v2024.0 images](https://images.apertis.org/release/v2024/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2024/v2024.0/amd64/fixedfunction/apertis_ostree_v2024-fixedfunction-amd64-uefi_v2024.0.img.gz) | [hmi](https://images.apertis.org/release/v2024/v2024.0/amd64/hmi/apertis_ostree_v2024-hmi-amd64-uefi_v2024.0.img.gz) | [base SDK](https://images.apertis.org/release/v2024/v2024.0/amd64/basesdk/apertis_v2024-basesdk-amd64-sdk_v2024.0.ova) | [SDK](https://images.apertis.org/release/v2024/v2024.0/amd64/sdk/apertis_v2024-sdk-amd64-sdk_v2024.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024/v2024.0/armhf/fixedfunction/apertis_ostree_v2024-fixedfunction-armhf-uboot_v2024.0.img.gz) | [hmi](https://images.apertis.org/release/v2024/v2024.0/armhf/hmi/apertis_ostree_v2024-hmi-armhf-uboot_v2024.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024/v2024.0/arm64/fixedfunction/apertis_ostree_v2024-fixedfunction-arm64-uboot_v2024.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2024/v2024.0/arm64/fixedfunction/apertis_ostree_v2024-fixedfunction-arm64-rpi64_v2024.0.img.gz) | [hmi](https://images.apertis.org/release/v2024/v2024.0/arm64/hmi/apertis_ostree_v2024-hmi-arm64-rpi64_v2024.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "virtualbox.md" >}} ).

#### Apertis v2024 package list

The full list of packages available from the v2024 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2024](https://infrastructure.pages.apertis.org/dashboard/tsv/v2024.tsv)

#### Apertis v2024.0 repositories

    deb https://repositories.apertis.org/apertis/ v2024 target development sdk non-free

## New features

### New versions of core packages

As part of the rebase on top of Debian Bookworm all the packages have been updated. As a summary this is a list
of the packages available:

- [GCC 12.2.0-14](https://gitlab.apertis.org/pkg/gcc-12/-/tree/apertis/v2024)
- [Glib 2.74.6-2](https://gitlab.apertis.org/pkg/glib2.0/-/tree/apertis/v2024)
- [Glibc 2.36-9+deb12u4](https://gitlab.apertis.org/pkg/glibc/-/tree/apertis/v2024)
- [Systemd 252.19-1~deb12u1](https://gitlab.apertis.org/pkg/systemd/-/tree/apertis/v2024)
- [OpenSSL 3.0.11-1~deb12u2](https://gitlab.apertis.org/pkg/openssl/-/tree/apertis/v2024)
- [GNU TLS 3.7.9-2+deb12u1](https://gitlab.apertis.org/pkg/gnutls28/-/tree/apertis/v2024)
- [Nginx 1.22.1-9](https://gitlab.apertis.org/pkg/nginx/-/tree/apertis/v2024)
- [Gstreamer 1.22.0-2](https://gitlab.apertis.org/pkg/gstreamer1.0/-/tree/apertis/v2024)
- [Pipewire 0.3.84-1~bpo12+1](https://gitlab.apertis.org/pkg/pipewire/-/tree/apertis/v2024)

The [Debian release notes](https://www.debian.org/releases/bookworm/releasenotes) provides additional information
about the new version of packages.

### Add support for podman

As working with different container technologies provides extra flexibility to build products, Apertis added
the support for [podman](https://gitlab.apertis.org/pkg/libpod). During this work, special care has been taken to ensure that this package and
its dependencies match [Apertis license expectations]({{< ref license-expectations >}}).

### Include user friendly license reports for target images

As part of the evolution of Apertis, a new set of reports based on [OSS Review Toolkit](https://github.com/oss-review-toolkit/ort)
are included for every target image. These reports are human friendly, allowing an easy review
of the licenses used by Apertis images.

### Include builddeps reports

Following the idea of providing more resources for Software Build of Materials, now Apertis provides
a report for the build dependencies of every package in the target images, including its version.
This information helps to easily track CVEs that might potentially affect Apertis packages.

### Support for running integration tests on MRs

Following the guidelines described in the [Apertis test strategy]({{< ref "test-strategy.md" >}})
this release provides initial support for running automated tests on LAVA in the
context of a MR. With this feature, packages changes can be validated by running integration tests
before landing them to main branches. 

It is currently recommended that only packages where potential regressions
may have a high-impact on the system stability have this feature enabled, given the extra
resources required to run the tests.

As before, all available integration tests are also run regularly on daily image builds.

### Additional compiler warnings enabled by default

This new releases includes an Apertis specific profile for `dpkg` to enable additional compiler warnings.
The use of this warnings can help to spot potential security issues in the packages being built,
improving the quality of the system.

## Build and integration

### Improve aptly integration and performance

After switching to aptly as the publisher for Apertis packages several improvements have been made
to the integration with OBS as well as the performance during publishing. As a result, the overhead
in the process of creating managing APT repositories has been dramatically reduced, both in the manual
effort required and in the amount of infrastructure resources.

### Support for more fine-grained access roles in QA Report App

With the goal of providing better access control to the QA Report App, now three different roles
are defined to allow performing the following actions: view results, submit manual tests results and
tag images. These changes should allow teams to better split responsibilities, by only allowing
the necessary access to specific users/groups. 

### Add support for package testing

This release includes a new way for developers to includes tests to run on a Merge Request.
The [package testing in LAVA]({{< ref "apertis-packages-testing.md" >}}) allows writing tests a part of the
standard packing metadata, which are used by our GitLab infrastructure to run LAVA tests to
ensure the proposed changes do not introduce any kind of regression in the supported hardware.

### Support for image change history

Apertis now provides [Change History Generator](https://gitlab.apertis.org/infrastructure/change-history-generator/)
a tool to generate a changelog between images. Thanks to this tool, as part of
[daily builds](https://images.apertis.org/daily/v2024/) a
[changelog](https://images.apertis.org/release/v2024/v2024.0/amd64/fixedfunction/ospack_v2024-amd64-fixedfunction_v2024.0.changelogs.tar.gz)
is created which helps developers to track changes across different images.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations.

### Breaks

No known breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds, can be found at <https://images.apertis.org/>.

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #339](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/339) Up Squared 6000 board fails to boot randomly in LAVA
- [Issue #363](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/363) aum-ota-rollback-blacklist: test failed
- [Issue #364](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/364) aum-offline-upgrade-branch: test failed
- [Issue #431](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/431) Package rust-coreutils does not provide license mapping information
- [Issue #437](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/437) Weekly images are not linked properly
- [Issue #438](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/438) pstore-test case is getting failed in armhf
- [Issue #451](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/451) dbus-installed-tests: test failed
- [Issue #456](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/456) SBOM support for rust packages does not work in v2024pre
- [Issue #466](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/466) AUM out of space tests fail sporadically
- [Issue #469](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/469) aum-power-cut: test failed
- [Issue #474](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/474) "WiFi WPA3-Transition-Mode access point" (AP mode) connection is not re-established after rebooting AP.
- [Issue #476](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/476) "apertis-update-manager-usb-unplug" is getting failed. Update is not starting automatically
- [Issue #480](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/480) aum-ota-api: test failed
- [Issue #485](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/485) AUM tests fail on v2024 and v2025dev* armhf
- [Issue #487](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/487) "sdk-vb-fullscreen" fails on "SDK" and "BASE-SDK" of "v2023.3rc1".
- [Issue #490](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/490) gpgv (sequioa) crashes when added debian archives
- [Issue #514](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/514) aum-out-of-space: test failed
- [Issue #523](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/523) Dev package name showing inside the target on target source list
- [Issue #526](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/526) Package centric testing does not work with OSTree images
- [Issue #528](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/528) OBS runner creates conflicts in the origin repo, then waits forever for them
- [Issue #530](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/530) aum-ota-out-of-space: test failed
- [Issue #532](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/532) AUM offline upgrade branch fails on RPi4
- [Issue #535](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/535) aum-ota-signed: test failed
- [Issue #537](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/537) Update some apertis gitlab-ci pipeline to use a bookworm image instead of a bullseye/buster image
- [Issue #539](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/539) Create automatic sdk tests in apertis-test-cases for abi-checker job

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #246](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/246) tiny-container-user-connectivity-profile: test failed
- [Issue #273](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/273) Debos build output hidden on GitLab job view can only be seen in the raw log file
- [Issue #322](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/322) wrap-and-sort fails to parse some debian/control.in files
- [Issue #330](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/330) datefudge: 64-bit time_t support on 32-bit archs
- [Issue #338](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/338) psdk-test not executing properly.
- [Issue #354](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/354) apparmor utils test shows as incomplete
- [Issue #376](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/376) apparmor functional demo reports errors
- [Issue #377](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/377) import-debian-package issues during package import
- [Issue #378](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/378) apparmor-gstreamer1-0: test failed
- [Issue #396](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/396) Pipeline to build package fails during build-source for texlive-extra
- [Issue #397](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/397) Pipeline to import new version of plotly from Bookworm fails during pull-updates
- [Issue #405](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/405) traprain: test failed
- [Issue #413](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/413) obs-runner: Disabled architectures on a OBS package get re-enabled when CI creates a branch
- [Issue #441](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/441) tiny-container-user-device-sharing: test failed
- [Issue #448](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/448) ade-commands: test failed
- [Issue #459](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/459) tiny-container-system-device-sharing: test failed
- [Issue #486](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/486) "webkit2gtk-ac-3d-rendering" fails in "ARMHF".
- [Issue #488](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/488) Flashing an HMI image on the eMMC of R-car H3e-2G board leads to a kernel panic.
- [Issue #491](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/491) AM62x: Do not install firmware prerequisites in image for boot firmware generation
- [Issue #508](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/508) sdk-import-debian-package: test failed
- [Issue #513](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/513) apparmor-ofono: test failed
- [Issue #525](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/525) RPi image list points to v2022 instead of daily development images

### Low
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #65](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/65) Terminal at xfce desktop corrupted after debos call

