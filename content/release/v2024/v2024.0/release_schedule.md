+++
date = "2023-12-05"
weight = 100

title = "v2024.0 Release schedule"

aliases = [
    "/release/v2024.0/release_schedule",
]
+++

The v2024.0 release cycle is scheduled to start in October 2023.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2024-01-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2024-02-07        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2024-02-14        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2024-02-21        |
| RC testing                                                                                               | 2024-02-22..03-06 |
| v2024.0 release                                                                                          | 2024-03-07        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
