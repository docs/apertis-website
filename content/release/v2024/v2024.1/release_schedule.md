+++
date = "2024-03-07"
weight = 100

title = "v2024.1 Release schedule"

aliases = [
    "/release/v2024.1/release_schedule",
]
+++

The v2024.1 release cycle is scheduled to start in October 2023.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2024-04-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2024-05-08        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2024-05-15        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2024-05-22        |
| RC testing                                                                                               | 2024-05-23..06-05 |
| v2024.1 release                                                                                          | 2024-06-06        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
