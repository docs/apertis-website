+++
date = "2024-06-03"
weight = 100

title = "v2024.1 Release Notes"

aliases = [
    "/release/v2024.1/releasenotes",
]
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2024.1** is the second **stable** release of the Apertis
v2024 stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2024 release stream up to the end
of 2025.

This is the first Apertis release that is built on top of Debian Bookworm
along with several customizations and it ships the latest Linux LTS kernel
6.6.x series. Later releases in the v2024 channel will be tracking kernel updates
in this LTS series as well as Debian Bookworm Stable Channel.

Test results for the v2024.1 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2024/20240522.1525/apt)
  - [OSTree images](https://qa.apertis.org/report/v2024/20240522.1525/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2024/20240522.1525/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2024/20240522.1525/lxc)

## Release flow

  - 2022 Q4: v2024dev0
  - 2023 Q1: v2024dev1
  - 2023 Q2: v2024dev2
  - 2023 Q3: v2024dev3
  - 2023 Q4: v2024pre
  - 2024 Q1: v2024.0
  - **2024 Q2: v2024.1**
  - 2024 Q3: v2024.2
  - 2024 Q4: v2024.3
  - 2025 Q1: v2024.4
  - 2025 Q2: v2024.5
  - 2025 Q3: v2024.6
  - 2025 Q4: v2024.7

### Release downloads

| [Apertis v2024.1 images](https://images.apertis.org/release/v2024/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2024/v2024.1/amd64/fixedfunction/apertis_ostree_v2024-fixedfunction-amd64-uefi_v2024.1.img.gz) | [hmi](https://images.apertis.org/release/v2024/v2024.1/amd64/hmi/apertis_ostree_v2024-hmi-amd64-uefi_v2024.1.img.gz) | [base SDK](https://images.apertis.org/release/v2024/v2024.1/amd64/basesdk/apertis_v2024-basesdk-amd64-sdk_v2024.1.ova) | [SDK](https://images.apertis.org/release/v2024/v2024.1/amd64/sdk/apertis_v2024-sdk-amd64-sdk_v2024.1.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024/v2024.1/armhf/fixedfunction/apertis_ostree_v2024-fixedfunction-armhf-uboot_v2024.1.img.gz) | [hmi](https://images.apertis.org/release/v2024/v2024.1/armhf/hmi/apertis_ostree_v2024-hmi-armhf-uboot_v2024.1.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024/v2024.1/arm64/fixedfunction/apertis_ostree_v2024-fixedfunction-arm64-uboot_v2024.1.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2024/v2024.1/arm64/fixedfunction/apertis_ostree_v2024-fixedfunction-arm64-rpi64_v2024.1.img.gz) | [hmi](https://images.apertis.org/release/v2024/v2024.1/arm64/hmi/apertis_ostree_v2024-hmi-arm64-rpi64_v2024.1.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "virtualbox.md" >}} ).

#### Apertis v2024 package list

The full list of packages available from the v2024 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2024](https://infrastructure.pages.apertis.org/dashboard/tsv/v2024.tsv)

#### Apertis v2024 repositories

    deb https://repositories.apertis.org/apertis/ v2024 target development sdk non-free

## New features

### Improved SBOM for licensing

Continuing with the work of improving the SBOM for licensing, now the report includes
license and copyright information for non binary files, such as scripts and configuration.
With this new enhancement Apertis' SBOM provides more accurate information about licensing
for the standard images.

## Build and integration

### Test on MR configuration

Running test on MR has shown to be a key new feature in Apertis, allowing both developers
and maintainers to ensure the sanity of the changes before they land. Following this idea
different types of test are available, such as
[package centric]( {{< ref "apertis-packages-testing.md" >}} ),
[integration and bootstrap test]( {{< ref "component_guide.md#testing-on-merge-requests" >}} ).
To improve readability and avoid misunderstandings, the configuration
settings were adapted, becoming more natural.

### Test on MR for linux

Continuing with the trend on adding test on MR, a set of custom tests have been implemented
for the linux kernel. As this is a very special package with delta respect to Debian
additional checks are now performed, to ensure that changes proposed by developers don't
cause regression and simplifying the review and test process.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations.

### Breaks

#### Package libgiod v2

In order to overcome limitations in libgiod v1 (1.6.3) it has been upgraded to its new
upstream release v2 (2.1-1). This change introduces small API changes that need to be addressed by developers.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds, can be found at <https://images.apertis.org/>.

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #364](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/364) aum-offline-upgrade-branch: test failed
- [Issue #419](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/419) High level tracking of AUM issues
- [Issue #431](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/431) Package rust-coreutils does not provide license mapping information
- [Issue #485](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/485) AUM tests fail on v2024 and v2025dev* armhf
- [Issue #490](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/490) gpgv (sequioa) crashes when added debian archives
- [Issue #567](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/567) AUM tests fail on RPi4 due to lack of space
- [Issue #573](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/573) aum-ota-auto: test failed
- [Issue #581](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/581) aum-offline-upgrade-signed: test failed
- [Issue #582](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/582) connman: test failed
- [Issue #587](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/587) aum-out-of-space: test failed

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #36](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/36) dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #246](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/246) tiny-container-user-connectivity-profile: test failed
- [Issue #273](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/273) Debos build output hidden on GitLab job view can only be seen in the raw log file
- [Issue #322](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/322) wrap-and-sort fails to parse some debian/control.in files
- [Issue #330](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/330) datefudge: 64-bit time_t support on 32-bit archs
- [Issue #339](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/339) Up Squared 6000 board fails to boot randomly in LAVA
- [Issue #377](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/377) import-debian-package issues during package import
- [Issue #396](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/396) Pipeline to build package fails during build-source for texlive-extra
- [Issue #397](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/397) Pipeline to import new version of plotly from Bookworm fails during pull-updates
- [Issue #405](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/405) traprain: test failed
- [Issue #413](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/413) obs-runner: Disabled architectures on a OBS package get re-enabled when CI creates a branch
- [Issue #441](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/441) tiny-container-user-device-sharing: test failed
- [Issue #448](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/448) ade-commands: test failed
- [Issue #459](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/459) tiny-container-system-device-sharing: test failed
- [Issue #486](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/486) "webkit2gtk-ac-3d-rendering" fails in "ARMHF".
- [Issue #488](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/488) Flashing an HMI image on the eMMC of R-car H3e-2G board leads to a kernel panic.
- [Issue #491](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/491) AM62x: Do not install firmware prerequisites in image for boot firmware generation
- [Issue #525](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/525) RPi image list points to v2022 instead of daily development images
- [Issue #528](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/528) OBS runner creates conflicts in the origin repo, then waits forever for them
- [Issue #537](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/537) Update some apertis gitlab-ci pipeline to use a bookworm image instead of a bullseye/buster image
- [Issue #539](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/539) Create automatic sdk tests in apertis-test-cases for abi-checker job
- [Issue #541](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/541) QA Report App is not able to login
- [Issue #554](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/554) import-debian-package doesn't trigger the pipeline adding a debian/apertis/copyright file
- [Issue #578](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/578) qa report application creates multiple entries per issue
- [Issue #586](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/586) sdk-import-debian-package: test failed
- [Issue #588](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/588) apparmor-ofono: test failed
- [Issue #597](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/597) obs runner is confused by multple OBS repos

### Low
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #65](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/65) Terminal at xfce desktop corrupted after debos call

