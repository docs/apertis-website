+++
date = "2024-11-28"
weight = 100

title = "v2024.3 Release Notes"

aliases = [
    "/release/v2024.3/releasenotes",
]
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2024.3** is the fourth **stable** release of the Apertis
v2024 stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2024 release stream up to the end
of 2025.

This is the first Apertis release that is built on top of Debian Bookworm
along with several customizations and it ships the latest Linux LTS kernel
6.6.x series. Later releases in the v2024 channel will be tracking kernel updates
in this LTS series as well as Debian Bookworm Stable Channel.

Test results for the v2024.3 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2024/20241113.0116/apt)
  - [OSTree images](https://qa.apertis.org/report/v2024/20241113.0116/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2024/20241113.0116/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2024/20241113.0116/lxc)

## Release flow

  - 2022 Q4: v2024dev0
  - 2023 Q1: v2024dev1
  - 2023 Q2: v2024dev2
  - 2023 Q3: v2024dev3
  - 2023 Q4: v2024pre
  - 2024 Q1: v2024.0
  - 2024 Q2: v2024.1
  - 2024 Q3: v2024.2
  - **2024 Q4: v2024.3**
  - 2025 Q1: v2024.4
  - 2025 Q2: v2024.5
  - 2025 Q3: v2024.6
  - 2025 Q4: v2024.7

### Release downloads

| [Apertis v2024.3 images](https://images.apertis.org/release/v2024/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2024/v2024.3/amd64/fixedfunction/apertis_ostree_v2024-fixedfunction-amd64-uefi_v2024.3.img.gz) | [hmi](https://images.apertis.org/release/v2024/v2024.3/amd64/hmi/apertis_ostree_v2024-hmi-amd64-uefi_v2024.3.img.gz) | [base SDK](https://images.apertis.org/release/v2024/v2024.3/amd64/basesdk/apertis_v2024-basesdk-amd64-sdk_v2024.3.ova) | [SDK](https://images.apertis.org/release/v2024/v2024.3/amd64/sdk/apertis_v2024-sdk-amd64-sdk_v2024.3.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024/v2024.3/armhf/fixedfunction/apertis_ostree_v2024-fixedfunction-armhf-uboot_v2024.3.img.gz) | [hmi](https://images.apertis.org/release/v2024/v2024.3/armhf/hmi/apertis_ostree_v2024-hmi-armhf-uboot_v2024.3.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2024/v2024.3/arm64/fixedfunction/apertis_ostree_v2024-fixedfunction-arm64-uboot_v2024.3.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2024/v2024.3/arm64/fixedfunction/apertis_ostree_v2024-fixedfunction-arm64-rpi64_v2024.3.img.gz) | [hmi](https://images.apertis.org/release/v2024/v2024.3/arm64/hmi/apertis_ostree_v2024-hmi-arm64-rpi64_v2024.3.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "virtualbox.md" >}} ).

#### Apertis v2024 package list

The full list of packages available from the v2024 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2024](https://infrastructure.pages.apertis.org/dashboard/tsv/v2024.tsv)

#### Apertis v2024 repositories

    deb https://repositories.apertis.org/apertis/ v2024 target development sdk non-free

## Changes

This is a point release in the stable cycle, only security fixes and
small changes are appropriate for this release stream.

This release includes the security updates from Debian Bookworm and the latest
LTS Linux kernel on the 6.6.x series.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations.

### Breaks

No nown breaks.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2024-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds, can be found at <https://images.apertis.org/>.

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #419](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/419) High level tracking of AUM issues
- [Issue #615](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/615) aum-offline-upgrade-branch: test failed
- [Issue #641](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/641) aum-ota-rollback-blacklist: test failed
- [Issue #642](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/642) aum-rollback-blacklist: test failed
- [Issue #671](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/671) Package rust-coreutils does not provide external reference files
- [Issue #677](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/677) aum-ota-out-of-space: test failed
- [Issue #688](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/688) aum-offline-upgrade-signed: test failed
- [Issue #690](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/690) aum-out-of-space: test failed

### Normal
- [Issue #7](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/7) Crash when initialising egl on ARM target
- [Issue #9](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/9) System users are shipped in /usr/etc/passwd instead of /lib/passwd
- [Issue #32](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/32) Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
- [Issue #49](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/49) "firmware: failed to load" logs seen during boot
- [Issue #66](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/66) Some binaries in the toolchain tarball are huge
- [Issue #273](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/273) Debos build output hidden on GitLab job view can only be seen in the raw log file
- [Issue #322](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/322) wrap-and-sort fails to parse some debian/control.in files
- [Issue #330](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/330) datefudge: 64-bit time_t support on 32-bit archs
- [Issue #339](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/339) Up Squared 6000 board fails to boot randomly in LAVA
- [Issue #377](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/377) import-debian-package issues during package import
- [Issue #396](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/396) Pipeline to build package fails during build-source for texlive-extra
- [Issue #405](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/405) traprain: test failed
- [Issue #413](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/413) obs-runner: Disabled architectures on a OBS package get re-enabled when CI creates a branch
- [Issue #441](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/441) tiny-container-user-device-sharing: test failed
- [Issue #448](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/448) ade-commands: test failed
- [Issue #459](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/459) tiny-container-system-device-sharing: test failed
- [Issue #462](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/462) agl-compositor mute functionality not working
- [Issue #485](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/485) AUM tests fail on v2024 for armhf
- [Issue #486](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/486) "webkit2gtk-ac-3d-rendering" fails in "ARMHF".
- [Issue #488](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/488) Flashing an HMI image on the eMMC of R-car H3e-2G board leads to a kernel panic.
- [Issue #490](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/490) gpgv (sequioa) crashes when added debian archives
- [Issue #491](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/491) AM62x: Do not install firmware prerequisites in image for boot firmware generation
- [Issue #528](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/528) OBS runner creates conflicts in the origin repo, then waits forever for them
- [Issue #537](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/537) Update some apertis gitlab-ci pipeline to use a bookworm image instead of a bullseye/buster image
- [Issue #539](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/539) Create automatic sdk tests in apertis-test-cases for abi-checker job
- [Issue #554](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/554) import-debian-package doesn't trigger the pipeline adding a debian/apertis/copyright file
- [Issue #597](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/597) obs runner is confused by multple OBS repos
- [Issue #602](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/602) lintian: the child pipeline lintian-errors is always triggered on the default branch instead of the same branch as the parent job
- [Issue #603](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/603) AUM upgrade branch fails on amd64 on all releases
- [Issue #614](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/614) sdk-cross-compilation: test failed
- [Issue #633](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/633) dashboard: issues just after a branching
- [Issue #637](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/637) Improve the repo/pipeline used to test the lintian job
- [Issue #638](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/638) Improve the repos/pipelines used to test the abi-checker job
- [Issue #639](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/639) apertis-dev depends on eatmydata that was dropped during the rebase on Bookworm
- [Issue #643](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/643) Improve the tool import-debian-package to trigger the generation of debian/apertis/copyright on an initial import.
- [Issue #652](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/652) sdk-import-debian-package: test failed
- [Issue #656](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/656) Improve apertis-pkg-* tools (from apertis-dev-tools) to use python-gitlab instead of relying on urllib
- [Issue #659](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/659) Remove "hmi" repository from website documentation
- [Issue #664](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/664) Improve workflow for importing packages
- [Issue #665](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/665) Write a python tool to generate apertis-oslist.json (for rpi-imager)
- [Issue #675](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/675) pkg/debugpy: investigate tests failure at build time
- [Issue #678](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/678) Test pipeline of ci-flatdeb-builder triggers too many (5) MR pipelines
- [Issue #679](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/679) Warning when booting v2024.3 RC1 OSTree Fixed Function image for armhf
- [Issue #691](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/691) tiny-container-system-connectivity-profile: test failed
- [Issue #692](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/692) tiny-container-user-connectivity-profile: test failed

### Low
- [Issue #15](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/15) Fix the kernel command line generation in OSTRee for FIT image
- [Issue #23](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/23) Remote outputs and local output, buffer size combined in Maynard/GTK
- [Issue #65](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/65) Terminal at xfce desktop corrupted after debos call
- [Issue #607](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/607) Pipeline for sample application helloworld-https-client fails
