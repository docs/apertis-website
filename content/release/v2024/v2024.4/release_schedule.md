+++
date = "2024-11-28"
weight = 100

title = "v2024.4 Release schedule"

aliases = [
    "/release/v2024.4/release_schedule",
]
+++

The v2024.4 release cycle is scheduled to start in January 2025.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2025-01-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2025-01-29        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2025-02-05        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2025-02-12        |
| RC testing                                                                                               | 2025-02-13..02-26 |
| v2024.4 release                                                                                          | 2025-02-27        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
