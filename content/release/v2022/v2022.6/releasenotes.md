+++
date = "2023-08-31"
weight = 100

title = "v2022.6 Release Notes"

aliases = [
    "/release/v2022.6/releasenotes",
]
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2022.6** is the seventh **stable** release of the Apertis v2022
stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2022 release stream up to the end
of 2023.

This Apertis release is built on top of Debian Bullseye with several
customisations and the Linux kernel 5.15.x LTS series.

Test results for the v2022.6 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2022/20230816.0116/apt)
  - [OSTree images](https://qa.apertis.org/report/v2022/20230816.0116/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2022/20230816.0116/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2022/20230816.0116/lxc)

## Release flow

  - 2020 Q4: v2022dev0
  - 2021 Q1: v2022dev1
  - 2021 Q2: v2022dev2
  - 2021 Q3: v2022dev3
  - 2021 Q4: v2022pre
  - 2022 Q1: v2022.0
  - 2022 Q2: v2022.1
  - 2022 Q3: v2022.3
  - 2022 Q4: v2022.3
  - 2023 Q1: v2022.4
  - 2023 Q2: v2022.5
  - **2023 Q3: v2022.6**
  - 2023 Q4: v2022.7

### Release downloads

| [Apertis v2022.6 images](https://images.apertis.org/release/v2022/v2022.6) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2022/v2022.6/amd64/fixedfunction/apertis_ostree_v2022-fixedfunction-amd64-uefi_v2022.6.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.6/amd64/hmi/apertis_ostree_v2022-hmi-amd64-uefi_v2022.6.img.gz) | [base SDK](https://images.apertis.org/release/v2022/v2022.6/amd64/basesdk/apertis_v2022-basesdk-amd64-sdk_v2022.6.ova) | [SDK](https://images.apertis.org/release/v2022/v2022.6/amd64/sdk/apertis_v2022-sdk-amd64-sdk_v2022.6.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.6/armhf/fixedfunction/apertis_ostree_v2022-fixedfunction-armhf-uboot_v2022.6.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.6/armhf/hmi/apertis_ostree_v2022-hmi-armhf-uboot_v2022.6.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.6/arm64/fixedfunction/apertis_ostree_v2022-fixedfunction-arm64-uboot_v2022.6.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.6/arm64/fixedfunction/apertis_ostree_v2022-fixedfunction-arm64-rpi64_v2022.6.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.6/arm64/hmi/apertis_ostree_v2022-hmi-arm64-rpi64_v2022.6.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (UP Squared 6000)]( {{< ref "amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "virtualbox.md" >}} ).

#### Apertis v2022.6 package list

The full list of packages available from the v2022.6 APT repositories is available in tab-separated value (TSV) format for easy processing in spreadsheets:

* [v2022.6](https://infrastructure.pages.apertis.org/dashboard/tsv/)

#### Apertis v2022.6 repositories

    deb https://repositories.apertis.org/apertis/ v2022 target development sdk


## Changes

This is a point release in the stable cycle, only security fixes and
small changes are appropriate for this release stream.

This release includes the security updates from Debian Bullseye and the latest
LTS Linux kernel on the 5.15.x series.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations.

### Breaks

No known breaks.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2022 infrastructure repository](https://build.collabora.com/project/show/apertis:infrastructure:v2022)
provides packages for the required versions in Debian Bullseye:

    deb https://repositories.apertis.org/infrastructure-v2022/ bullseye infrastructure

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

### High
- [Issue #190](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/190) aum-power-cut: test failed
- [Issue #215](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/215) Follow up sporadic FS corruption in R car boards
- [Issue #339](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/339) Up Squared 6000 board fails to boot randomly
- [Issue #363](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/363) aum-ota-rollback-blacklist: test failed
- [Issue #365](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/365) aum-ota-auto: test failed
- [Issue #366](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/366) aum-ota-signed: test failed
- [Issue #368](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/368) aum-ota-api: test failed
- [Issue #371](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/371) aum-out-of-space: test failed
- [Issue #408](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/408) aum-offline-upgrade: test failed
- [Issue #410](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/410) aum-offline-upgrade-signed: test failed
- [Issue #411](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/411) aum-ota-out-of-space: test failed
- [Issue #419](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/419) High level tracking of AUM issues
- [Issue #425](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/425) Fonts Application (Fonts app) is not found in SDK image
- [Issue #426](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/426) aum-rollback-blacklist: test failed
- [Issue #427](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/427) Toolchain for arm32 and arm64 provide libraries for both architectures
- [Issue #428](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/428) connman: test failed
- [Issue #431](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/431) Package rust-coreutils does not provide license mapping information
- [Issue #434](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/434) aum-api: test failed

### Normal
- [Issue #9](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/9) System users are shipped in /usr/etc/passwd instead of /lib/passwd
- [Issue #49](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/49) "firmware: failed to load" logs seen during boot
- [Issue #66](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/66) Some binaries in the toolchain tarball are huge
- [Issue #114](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/114) ci-package-builder tests fail due to OBS branches not being cleaned up
- [Issue #118](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/118) tiny-container-system-connectivity-profile: test failed
- [Issue #219](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/219) Eclipse-ide-cpp can't show preview of markdown files (.md)
- [Issue #230](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/230) WebProcess CRASHED error is seen on executing webkit related testcases
- [Issue #246](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/246) tiny-container-user-connectivity-profile: test failed
- [Issue #252](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/252) tiny-container-system-basic: test failed
- [Issue #256](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/256) OBS: Backend doesn’t start immediately upon restart
- [Issue #273](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/273) Debos build output hidden on GitLab job view can only be seen in the raw log file
- [Issue #289](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/289) Difference in license data observation between scan copyright and fossology tool data
- [Issue #329](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/329) ade-commands: test failed
- [Issue #331](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/331) Frequent 504 Errors repeatedly causing pipelines to be marked as failed
- [Issue #337](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/337) flatpak-run-demo-cli-app: test failed
- [Issue #338](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/338) psdk-test not executing properly.
- [Issue #340](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/340) sdk-import-debian-package: test failed
- [Issue #342](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/342) tiny-container-system-aa-enforcement: test failed
- [Issue #344](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/344) tiny-container-user-basic: test failed
- [Issue #357](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/357) Instability issue in  bluez-phone test case for Up Squared 6000 board
- [Issue #378](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/378) apparmor-gstreamer1-0: test failed
- [Issue #396](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/396) Pipeline to build package fails during build-source for texlive-extra
- [Issue #397](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/397) Pipeline to import new version of plotly from Bookworm fails during pull-updates
- [Issue #415](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/415) sdk-vb-fullscreen test case is failing on 2024dev3 image.
- [Issue #418](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/418) aum-offline-upgrade-branch fails on v2022
- [Issue #432](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/432) tiny-container-user-folder-sharing: test failed
- [Issue #433](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/433) sdk-performance-tools-gprof: test failed
- [Issue #435](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/435) tiny-container-system-seccomp: test failed

### Low
- [Issue #65](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/65) Terminal at xfce desktop corrupted after debos call
- [Issue #206](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/206) AUM rollback tests fail on UP Squared 6000 board
- [Issue #211](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/211) AUM power cut tests fail on UP Squared 6000 board
- [Issue #270](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues/270) Issue in playing the Video on HMI armhf
