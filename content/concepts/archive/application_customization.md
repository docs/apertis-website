+++
date = "2024-06-12"
weight = 100

title = "Application customization"

aliases = [
    "/concepts/application_customization",
]
+++

{{< section-toc >}}
