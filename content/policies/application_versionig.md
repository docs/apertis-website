+++
title = "Package versioning"
weight = 100
outputs = [ "html", "pdf-in",]
date = "2024-10-31"
+++

Apertis is an source distribution based on Debian. As such it follows the same rules as Debian for
the versioning of the packages and applications.

However, besides the general guidelines from upstream, it is useful to clarify different use cases
in order to provide clear policies.

For this reason, the current document describe different scenarios and how to deal with them.

## Update for Debian package

This is the most common case, and the simplest one. Apertis adds the suffix `+apertis` and an Apertis
revision starting form `0`. Apertis specific changes bumps the Apertis revision.

**Example:**

Starting point:
- Debian: dash 0.5.12-2
- Apertis: dash 0.5.12-2+apertis0 (v2025pre)

After update:
- Apertis: dash 0.5.12-2+apertis1 (v2025pre)

## Update for Apertis native packages

In this case, the upstream repo for the project is in Apertis, so a versioning scheme should be used
which aligns with the release cycle and makes it easier to plan backports. Taking this into account
Apertis uses a follow the `major.minor.patch` schema with `major=0`, `minor=release` and `patch=version`.
This election aims to make it easier to backports changes and diverge to adapt to different Apertis
release.

In case that the update is planned to be backported the version should only increment the patch number,
even in the case of new release.

**Example**

Starting point:
- Apertis apertis-dev-tools 0.2024.15 (v2025pre)
- Apertis apertis-dev-tools 0.2024.15 (v2024)

After update that is planned to be backported:
- Apertis apertis-dev-tools 0.2024.16 (v2025pre)
- Apertis apertis-dev-tools 0.2024.16 (v2024)

However, in case of update not planned to be backported, the minor number should increase and match the
release number.

**Example**

Starting point:
- Apertis apertis-dev-tools 0.2024.15 (v2025pre)
- Apertis apertis-dev-tools 0.2024.15 (v2024)

After update that is **not** planned to be backported:
- Apertis apertis-dev-tools 0.2025.0 (v2025pre)
- Apertis apertis-dev-tools 0.2024.15 (v2024)

## Backport of Debian update for Debian package

Due to the different release cycles between Debian and Apertis it is possible that the same version of the
package is found in multiple Apertis release. In this case the backport is straight forward as the exact
same version is applied in older releases.

However, due to the needs of each Apertis release, in some occasions, the backport is not straight forward
as the same Debian version of the package requires different version in different Apertis releases. In this
case a Apertis release suffix should be added.

**Example:**

Starting point:
- Debian: dash 0.5.12-1
- Apertis: dash 0.5.12-1+apertis1 (v2025pre)
- Apertis: dash 0.5.12-1+apertis0 (v2024)

After update:
- Debian: dash 0.5.12-2
- Apertis: dash 0.5.12-2+apertis0 (v2025pre)
- Apertis: dash 0.5.12-2+apertis0~v2024 (v2024)

## Backport of Apertis update for Debian package

In case of Apertis specific updates, there are different scenarios. The first one and simpler is that the
older release was matching the same version, in which case the exact same version will be applied. The second
one, is when there are different Debian version. This is also simple, in the sense that the update needs to be
applied on top of the older version and the Apertis revision needs to be increased.

**Example:**

Starting point:
- Apertis: dash 0.5.12-2+apertis0 (v2025pre)
- Apertis: dash 0.5.12-1+apertis0 (v2024)

After update:
- Apertis: dash 0.5.12-2+apertis1 (v2025pre)
- Apertis: dash 0.5.12-1+apertis1 (v2024)

The third case is when there version in the older release is the same Debian version but an older Apertis
revision. In this case we cannot increment the Apertis suffix in the older release since that version already
exits, so and addition suffix needs to be added.

**Example:**

Starting point:
- Apertis: dash 0.5.12-2+apertis1 (v2025pre)
- Apertis: dash 0.5.12-2+apertis0 (v2024)

After update:
- Apertis: dash 0.5.12-2+apertis2 (v2025pre)
- Apertis: dash 0.5.12-1+apertis1+v2024 (v2024)

## Reference
- [Debian policy for package versioning](https://www.debian.org/doc/debian-policy/ch-controlfields.html#version):
it contains a comprehensive explanation of how the versions work.
- [Debian Go policy for package versioning](https://go-team.pages.debian.net/packaging.html#_version_numbers):
it provides a guide for generating a version when upstream does not use version numbers (for instance, it's
common for Go libraries to have no version numbers in the traditional sense).