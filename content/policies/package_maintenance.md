+++
date = "2019-10-25"
weight = 100

title = "Package maintenance"

aliases = [
    "/old-wiki/Package_maintenance"
]
+++

Apertis hosts its own package subset on the
[Apertis GitLab instance](https://gitlab.apertis.org/). On successful
completion of the CI pipeline these are uploaded to
[Collabora's Open Build Service (OBS) instance](https://build.collabora.com)
where the packages are formally built and hosted for Apertis.

## Packages Guidelines

The package set is distributed in several groups: `target`,
`development`, `sdk`, `hmi`, `helper-libs`. Those groups are merged in
a single repository, and split in what distribution names repository
components (`target`, `development`, `sdk`, `hmi`, `helper-libs`).

Each of these repository components have different constraints regarding
the type of package that can be hosted. Check the
[licensing]( {{< ref "license-applying.md" >}} )
documentation for details.

  - Source packages modified from upstream distribution include a
    versioning suffix 'co'.
  - Source packages in SDK are built only for supported SDK
    architectures (Intel 64 bit)
  - Source packages in `target`, `development`, `hmi`, `helper-libs` are
    built for all supported architectures (ARM hard float, ARM 64bit,
    Intel 64 bit)

Target images contain `target`, `hmi` components. At build time, target
images can refer to `development` components.

## Requesting a new package

If a new package is desired, the request should be made following the relevant 
[process]( {{< ref "contributions.md#extending-apertis" >}} ).

## Exceptions to the package update policy

The vast majority of packages of a given Apertis release come from the based Debian release allowing
Apertis to benefit from the stability and security updates from Debian.
However, some packages have a different origin, this could be a newer Debian release,
Debian backports repositories or directly from the main project of the software component.

These packages need special attention since Apertis developers have to maintain
and update them themselves.

Here is the list of these packages with their update frequency in Apertis, which might change in the future to cover new use cases:
- `linux`: Each Apertis release comes with the latest LTS kernel available at
   the time of it's `v20xx.0` release as described in
   [Linux kernel release flow]( {{< ref "release-flow.md#linux-kernel-release-flow" >}} ).
   For instance, Apertis `v2023.x` provides linux LTS 6.1.x, whereas `v2024.x`
   targets linux LTS 6.6.x. To achieve that, we may have to pull the linux package
   from different Debian releases depending on how Debian tracks Linux releases.
   For instance, we may start by tracking the kernel from the Debian release N
   during early development, switch to the Debian backports kernel to start
   tracking newer kernels when they become available and switch to picking updates
   from Debian N+1 when the backports cease or move to a newer kernel release than
   we want. If the required kernel version is no longer provided in any Debian
   release it will be necessary to track the [www.kernel.org](https://www.kernel.org/)
   LTS kernel directly. In other words, the linux package remains as much as possible in sync
   with the Debian's one, until Debian no longer provides the targeted LTS series.
   Thus, it requires regular manual updates, this will be provided approximately once every
   two months.
- `u-boot`: During the development period of Apertis, this package is pulled from
   Debian/sid. This component has lower update frequency than the kernel.
   Updates for stable Apertis releases are on a case-by-case basis since any update
   of `u-boot` requires manual testing on [all supported boards]({{< ref "reference_hardware" >}}).
- `pipewire` & `wireplumber`: Both have rapid development, thus to benefit from
   the latest innovations, these packages are pulled from Debian `-backports`
   repositories to the development channel.
   Stable Apertis releases don't receive updates (only bug fix and security
   updates are allowed).
- `debos` and `golang-github-go-debos-fakemachine` both are pulled from newer
   Debian release on request to benefit from new features.
- `strace`: This package is in sync with `linux`. It's quite common that a major
   linux update breaks the strace build. When this occurs, a new version
   of strace matching the new linux version is imported from Debian.
- Some packages are **first contributed to Apertis** before being pushed into
   Debian. That means Apertis is ahead of Debian for a release until the usual
   workflow allows Apertis to resynchronize with Debian. In this case, updates
   in Apertis for releases ahead of Debian are made on a case-by-case basis
   depending on several factors like security issues, etc.
   - `onnxruntime`
   - `optee-os`
   - `optee-test`
- For some other packages, **Apertis is upstream of Debian**. Thus, the development
   channel of Apertis may be ahead of Debian Sid, but the policy is to keep
   Debian Sid in sync with latest versions of these packages.
   - `gitlab-rulez`
   - `dwarf2sources`
- Some packages are **not suitabe for Debian**. Updates for these packages are
   made on an as-needed basis:
   - `agl-compositor`: This package is not available in Debian, but is retrieved
      from the Automotive Grade Linux distribution (AGL). Only during the development
      period of Apertis, `agl-compositor` is synchronized with the version available
      in the latest AGL LTS as described in
      [agl-compositor/debian/README.source](https://gitlab.apertis.org/pkg/agl-compositor/-/blob/apertis/v2025dev3/debian/README.source).
      Stable Apertis releases don't receive updates (only bug fix and security
      updates are allowed).
   - `imx-firmware`: This package provides firmware required to support the board
      [i.MX8MN Variscite]( {{< ref "imx8mn_var_symphony_setup.md" >}} ) board. It
      was not contributed to Debian due to [redistribution issue](https://bugs.debian.org/1035055).
   - `k3-image-gen`: This package is used to build a boot firmware for the board
      [TI SK-AM62]( {{< ref "sk-am62_setup.md" >}} ) board. No longer required
      with recent version of `u-boot`.
   - `ti-firmware`: This package provides a firmware for the board
      [TI SK-AM62]( {{< ref "sk-am62_setup.md" >}} ) board. Not contribued to
      Debian since, TI needs to push it to `linux-firmware`.
   - `ltp`: this package was removed from Debian in 2012 because it was no longer
     maintained. We added it to Apertis, but it was not contributed to Debian.
   - `python-robotframework`,`python-robotframework-seriallibrary`,
     `python-robotframework-sshlibrary` and `python-robotframeworklexer`: all
     these packages are used to support [Robot Framework]( {{< ref "robotframework.md" >}} )
     but they were not contributed to Debian.
- Packages removed in latest Apertis releases, but still available in a supported
   Apertis release. Updates may occur only to fix security issues or targeted
   major issues:
   - `eclipse-ide-cpp`, removed after v2023. The current recommendation is to use `vscodium`.
   - `golang-github-eclipse-ditto-clients`, removed after v2023. Part of the
     *kanto* deprecation.
   - `golang-github-eclipse-kanto-suite-connector`, removed after v2023. Part of the
     *kanto* deprecation.
   - `golang-github-lithammer-shortuuid`, removed after v2023. Part of the
     *kanto* deprecation.
   - `golang-github-threedotslabs-watermill`, removed after v2023. Part of the
     *kanto* deprecation.
   - `golang-1.18`, removed after v2023 and no longer in any Debian release.
      Part of the *kanto* deprecation.
   - `libcroco`, removed after v2023 and Buster.
   - `libical`, removed after v2023 and Stretch.
   - `realpath`, removed after v2023 and Wheezy.
   - `rust-ioctl-sys`, removed after v2023, it was part of the dependency chain
     of `rust-coreutils`.
   - `vk-gl-cts-opengl-es-cts` removed after v2023.
