+++
date = "2019-11-15"
lastmod = "2024-06-28"
weight = 100

title = "Community"

aliases = [
    "/old-wiki/Community",
    "/policies/community"
]
+++

Apertis is a free software project which is designed and developed in
the open. The Apertis community is made up of both contributors who work
to improve Apertis and users.

## Join the community

The best ways to get in touch with the community are through:

  - [Mailing lists](https://lists.apertis.org/)
      - The [devel mailing
        list](https://lists.apertis.org/postorius/lists/devel.lists.apertis.org/)
        is the place to discuss Apertis development and project.
      - If you are interested in adding a new mailing list, please
        contact [listmaster@apertis.org](mailto:listmaster@apertis.org).
  - [Apertis GitLab](https://gitlab.apertis.org/)
      - It is where development and most technical discussions take place.

## Contact the Apertis team

The Apertis team can be contacted by sending an email to [contact@apertis.org](mailto:contact@apertis.org).
Unlike the links above, this email address is not public, only the Apertis team
can read emails sent to this address.

## Report issues

Apertis utilises issues on the [Apertis issues](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues)
GitLab repository as a bug tracking system. If you experience any misbehaviour please report the issue there, the Apertis
development team will do their best to help you.

## Report security issues

For security issues which requires discretion, you can either create a confidential issue in the
[Apertis issues](https://gitlab.apertis.org/infrastructure/apertis-issues/-/issues) repository or
contact the Apertis Security team at [security@apertis.org](mailto:security@apertis.org).

Remember that all contributors to Apertis are expected to [treat others
with respect]( {{< ref "code_of_conduct.md" >}} ).
