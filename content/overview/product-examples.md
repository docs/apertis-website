+++
date = "2024-07-01"
weight = 100

title = "Product examples"
+++

Apertis is used on industrial embedded devices available in the market.
Some examples of products using Apertis-based operating systems include:

# Atari VCS

The [Atari VCS](https://atari.com/pages/atari-vcs) is a game console allowing to
play, among other things, thousands of classics games. The device is also capable
of using a PC mode for multimedia or office purposes.

# Bosch Wallscanner D-tect 200

The [Bosch Wallscanner D-tect 200](https://www.bosch-professional.com/gb/en/products/wallscanner-d-tect-200-c-0601081600)
is a professional tool to scan walls and detect objects inside them preventing
damage during drilling, and measuring concrete coverage.
