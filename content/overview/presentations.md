+++
date = "2017-05-11"
lastmod = "2024-06-19"
weight = 100

title = "Presentations"
aliases = [
    "/architecture/presentations",
    "/architecture/references/presentations"
]
+++

These are some of the slides and presentations which members of the
Apertis community have previously shared (ordered newest to oldest):

# 2024

* [Apertis v2024: the new Bookworm-based release for industrial embedded devices](https://www.collabora.com/news-and-blog/news-and-events/apertis-v2024-the-new-bookworm-based-release-for-industrial-embedded-devices.html)
  - Announcement of the Apertis v2024 release with a summary of its new features.  
&nbsp;
* [Apertis - A Collaborative Industrial Grade Linux Construction Kit](https://www.youtube.com/watch?v=tnHI9fjSZ3Y)
  - Introduction to Apertis by Philipp Ahmann
  - [slides](/images/EOSS24-ELC-APERTIS-ahmann-upload.pdf)
  - Presented at the [EOSS NA 2024](https://eoss24.sched.com/)  
&nbsp;
* [Blast from the past at Embedded World: Atari plays for Linux](https://www.collabora.com/news-and-blog/news-and-events/blast-from-the-past-at-embedded-world-atari-plays-for-linux.html)
  - A modern take on creating an open GNU/Linux console: the Atari VCS
  - Booth at the [Embedded World 2024](https://www.embedded-world.de/en)

# 2023

* [Debian in your (future) car?](https://www.youtube.com/watch?v=5jzKZHPI-KA) (*in French*)
  - Introduction to Apertis (a Debian derivative) an industrial embedded systems
    by Arnaud Ferraris
  - [slides](/images/DebianEmbarque_CdL2023_1kJVAn8.pdf)
  - Presented at the [Capitole du Libre 2023](https://capitoledulibre.org/)  
&nbsp;
* [The AGL Wayland compositor](https://www.youtube.com/watch?v=WxdlwGParzg)
  - Short overview about *agl-compositor*, the [Wayland compositor
    used in Apertis]({{< ref "wayland_compositors.md" >}})
    by Marius Vlad
  - [slides](/images/EOSS2023_agl_compositor_marius_vlad.pdf)
  - Presented at the [EOSS 2023](https://eoss2023.sched.com/)

# 2022

* [Apertis & ELISA](https://www.youtube.com/watch?v=vmQUmqU42ac)
  - Introduction to Apertis and ELISA (*Enabling Linux In Safety Applications*)
    by Philipp Ahmann
  - [slides](/images/2022-05-13_LLC_Apertis_and_ELISA_final.pdf)
  - Presented at the [LLC 2022](https://lundlinuxcon.org/)

# 2021

* [A libweston-based compositor for Automotive Grade Linux](https://www.collabora.com/news-and-blog/news-and-events/a-libweston-based-compositor-for-automotive-grade-linux.html)
  - Introduction to *agl-compositor*, the [Wayland compositor
    used in Apertis]({{< ref "wayland_compositors.md" >}})
    by Marius Vlad

# 2020

* [Do We Need an Industrial Grade Linux?](https://www.youtube.com/watch?v=OJcUpP5b3WY)
  - Introduction to Apertis for embedded devices by Lars Geyer-Blaumeiser
  - [slides](/images/20201009OSSEUDoWeNeedAnIndustrialGradeLinux.pdf)
  - [Panel Discussion Follow-up: Do We Need an Industrial Grade Linux?](https://www.youtube.com/watch?v=j3s6t2pqHEA)
  - Presented at the [Open Source Summit 2020](https://www.accelevents.com/e/OSSELCEU2020)  
&nbsp;
* [Creating Debian-Based Embedded Systems using Debos](https://www.youtube.com/watch?v=8HijkXwpFxc)
  - The tool developed to build Apertis' images by Christopher Obbard
  - Presented at the [Live Embedded Event 2020](https://live-embedded-event.carrd.co/)  
&nbsp;
* [Creating Debian-Based Embedded Systems in the Cloud Using Debos](https://www.youtube.com/watch?v=57Lmv1hG9T0)
  - The tool developed to build Apertis' images by Christopher Obbard
  - Presented at the [Embedded Linux Conference Europe 2020](https://events.linuxfoundation.org/archive/2020/embedded-linux-conference-europe/)  
&nbsp;
* [OSTree CLI for OS management](https://www.youtube.com/watch?v=B0xvrXkEwr4)
  - How to manage OS upgrades with OSTree CLI by Denis Pynkin
  - [slides](https://www.collabora.com/assets/pdfs/OSTree_CLI_for_OS_management.pdf)

# 2019

* [Building an entire Debian derivative from Git](https://meetings-archive.debian.net/pub/debian-meetings/2019/DebConf19/building-a-debian-derivative-from-git.webm)
  - The Apertis approach to using git as part of the build pipeline by Andrej
    Shadura
  - [slides](https://shadura.me/talks/debconf/2019/distro-in-git)
  - Presented at the [DebConf19](https://debconf19.debconf.org/)  
&nbsp;
* [Apertis OS for embedded  devices](https://www.youtube.com/watch?v=The0nz7nSsk) (*in Russian*)
  - Overview of Apertis itself and some key components by Denis Pynkin
  - [slides](/images/Apertis_OS_compressed.pdf)
  - Presented at the [LVEE 2019](https://lvee.org/en/reports/materials_lvee_2019)

# 2018

* [Testing your distribution with LAVA](https://www.youtube.com/watch?v=Yyoq8hQdutE)
  - Introducton to testing with LAVA by Andrej Shadura and Guillaume Tucker
  - [slides](https://shadura.me/talks/lvee/2018/testing-your-distribution-with-lava)
  - Presented at the [LVEE 2018](https://lvee.org/en/reports/materials_lvee_2018)

# 2017

* [Managing build infrastructure for a Debian derivative](https://www.youtube.com/watch?v=xuHKOrs_1GY)
  - Introduction on the build and integration infrastructure of Apertis by
    Andrew Shadura
  - Presented at the [DebConf17](https://debconf17.debconf.org/)  
&nbsp;
* [Application Framework APIs in action - a case study](/images/genivi-amm-2017-slides.odp)
  - Introduction to application framework for GENIVI and Apertis by Ekaterina
    Gerasimova and Andre Magalhães
  - Presented at the *GENIVI All Member Meeting 2017*  
&nbsp;
* [A Debian maintainer's guide to Flatpak](https://www.youtube.com/watch?v=9SmZYQAqBd8)
  - An overview of how Flatpak and its portals behave and how they’re implemented by
    Simon McVittie
  - Presented at the [DebConf17](https://debconf17.debconf.org/)

# 2016

* [Building an automotive platform from GNOME](https://media.ccc.de/v/53-building_an_automotive_platform_from_gnome)
  - An introduction to Apertis by Lukas Nack
  - Presented at [GUADEC 2016](https://2016.guadec.org/)  
&nbsp;
* [Introduction to Apertis](/images/apertis-gnomeasia-2016-slides.odp)
  - An overview of architecture and development by Sudarshan Chikkapura
    Puttalingaiah
  - Presented at *GNOME.Asia Summit 2016*
